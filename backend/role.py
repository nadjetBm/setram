from flask import Flask, render_template, redirect, url_for, jsonify
import psycopg2
from login import *
from profil import *
from rejeu import *
from role import *
from flask_cors import CORS
from database import conn
from datetime import timedelta
from datetime import datetime


app = Flask(__name__)
def create_role():
    data = request.get_json()
    role_name = data['roleName']
    rejeu = data['rejeu']
    frequentation =  data['frequentation']
    exploitation = data['exploitation']
    parcours = data['parcours']
    suivi = data['suivi']

    # Vérification si le nom de rôle existe déjà
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM role WHERE role_name = %s", (role_name,))
    existing_role = cursor.fetchone()

    if existing_role:
        # Le nom de rôle existe déjà, renvoyer une réponse d'erreur
        response = {
            'success': False,
            'message': 'Role already exists.'
        }
        return jsonify(response), 400

    # Le nom de rôle n'existe pas, procéder à l'insertion
    cursor.execute(
        "INSERT INTO role (role_name, rejeu, frequentation, exploitation, parcours, suivi) VALUES (%s, %s, %s, %s, %s, %s)",
        (role_name, rejeu, frequentation, exploitation, parcours, suivi)
    )
    conn.commit()
    cursor.close()

    # Renvoyer une réponse JSON indiquant le succès
    response = {
        'success': True,
        'message': 'Role created successfully.'
    }
    return jsonify(response)
#################
def delete_role(role_name):
    try:
        cursor = conn.cursor()
        delete_query = "DELETE FROM role WHERE role_name = %s"
        cursor.execute(delete_query, (role_name,))
        conn.commit()
        cursor.close()
        return jsonify({'message': 'Le rôle a été supprimé avec succès!'})
    except Exception as e:
        return jsonify({'error': str(e)}), 500


def display_components():
    if 'logged_in' in session:
        role_compte = session['role_compte']
        cursor = conn.cursor()
        cursor.execute("SELECT rejeu, frequentation, exploitation,parcours,suivi FROM role WHERE role_name=%s", (role_compte,))
        role_data = cursor.fetchone()

        if role_data:
            rejeu, frequentation, exploitation,parcours,suivi= role_data
            return jsonify({'rejeu': rejeu, 'frequentation':  frequentation, 'exploitation':  exploitation, 'parcours': parcours, 'suivi': suivi})
        else:
            return jsonify({'message': "No role found for the logged-in user."})
    else:
        return jsonify({'message': "User not logged in"})
    
    ################
def get_role():
    cur = conn.cursor()
    cur.execute("SELECT * FROM role")
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"role_name": row[1]}
        data_dict["rejeu"] = row[2]
        data_dict["frequentation"] = row[3]
        data_dict["exploitation"] = row[4]
        data_dict["parcours"] = row[5]
        data_dict["suivi"] = row[6]
        data.append(data_dict)

    return jsonify(data)
   ##########
def modifier_role(roleName):
    cur = conn.cursor()
    role_query = "UPDATE role SET rejeu = %s, frequentation = %s, exploitation = %s, parcours = %s, suivi = %s WHERE role_name = %s"
    cur.execute(role_query, (
        request.json['rejeu'],
        request.json['frequentation'],
        request.json['exploitation'],
        request.json['parcours'],
        request.json['suivi'],
        roleName
    ))
    conn.commit()
    cur.close()
    return jsonify({'message': 'Le rôle a été modifié avec succès!'})

   


