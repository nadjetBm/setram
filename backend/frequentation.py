from flask import Flask, render_template, redirect, url_for, jsonify
from login import login
from rejeu import *
from flask import Flask, jsonify
from flask_cors import CORS
from database import conn
from datetime import timedelta
from datetime import datetime




app = Flask(__name__)

######################################## nbJour######

def get_progress_bar_data(date, selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT SUM(nombre_validation) AS nombre_validation FROM validation WHERE date_tram = %s AND sens = %s", (date,selectedVoie, ))
    result = cur.fetchone()
    data = {"nombre_validation": result[0]}
    return jsonify(data)


def get_validation_data(date, selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT SUM(nombre_validation) AS nombre_validation, station_validation FROM validation WHERE date_tram = %s AND sens = %s GROUP BY station_validation ", (date, selectedVoie,))
    result = cur.fetchall()
    data = [{"station_validation": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)


def get_horaire_data(date, selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT SUM(nombre_validation) AS nombre_validation, tranche_horaire FROM validation WHERE date_tram = %s AND sens = %s GROUP BY tranche_horaire Order by tranche_horaire ", (date, selectedVoie,))
    result = cur.fetchall()
    data = [{"tranche_horaire": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)


######################################## MoyJour######

def get_moybar(date, selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT SUM(avg_nombre_validation) FROM (SELECT AVG(nombre_validation) as avg_nombre_validation FROM validation WHERE date_tram = %s AND sens = %s GROUP BY tram) as subquery;", (date,selectedVoie, ))
    result = cur.fetchone()
    data = {"nombre_validation": result[0]}
    return jsonify(data)


def get_station_data(date, selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT AVG(nombre_validation) AS nombre_validation, station_validation FROM validation WHERE date_tram = %s AND sens = %s GROUP BY station_validation ", (date, selectedVoie,))
    result = cur.fetchall()
    data = [{"station_validation": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)


def get_moy_horaire_data(date, selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT AVG(nombre_validation) AS nombre_validation, tranche_horaire FROM validation WHERE date_tram = %s AND sens = %s GROUP BY tranche_horaire Order by tranche_horaire ", (date, selectedVoie,))
    result = cur.fetchall()
    data = [{"tranche_horaire": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)

######################################## nbPeriode######

def get_nbPeriode_progress(startDate, endDate, selectedVoie):
    cur = conn.cursor()
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur.execute("SELECT SUM(nombre_validation) AS total_validations FROM validation WHERE date_tram BETWEEN %s AND %s AND sens = %s;", (start_date, end_date, selectedVoie,))
    result = cur.fetchone()
    cur.close()
    if result is None:
        return jsonify({"error": "No data available for the specified period."}), 404
    total_validations = result[0]
    data = {"nombre_validation": total_validations}
    return jsonify(data)


def get_nbPeriode_bar(startDate, endDate, selectedVoie):
    cur = conn.cursor()
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur.execute("SELECT SUM(nombre_validation) AS nombre_validation, station_validation FROM validation WHERE date_tram BETWEEN %s AND %s AND sens = %s GROUP BY station_validation;", (start_date, end_date, selectedVoie))
    result = cur.fetchall()
    data = [{"station_validation": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)


def get_nbPeriode_line(startDate, endDate, selectedVoie):
    cur = conn.cursor()
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur.execute("SELECT SUM(nombre_validation) AS nombre_validation, tranche_horaire FROM validation WHERE date_tram BETWEEN %s AND %s AND sens = %s GROUP BY tranche_horaire Order by tranche_horaire; ",(start_date, end_date, selectedVoie))
    result = cur.fetchall()
    data = [{"tranche_horaire": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)





######################################## moyPeriode######

def get_moyPeriode_progress(startDate, endDate, selectedVoie):
    cur = conn.cursor()
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur.execute("SELECT SUM(avg_nombre_validation) FROM (SELECT AVG(nombre_validation) as avg_nombre_validation FROM validation WHERE date_tram BETWEEN %s AND %s AND sens = %s GROUP BY tram) as subquery;", (start_date, end_date, selectedVoie,))
    result = cur.fetchone()
    cur.close()
    if result is None:
        return jsonify({"error": "No data available for the specified period."}), 404
    total_validations = result[0]
    data = {"nombre_validation": total_validations}
    return jsonify(data)


def get_moyPeriode_bar(startDate, endDate, selectedVoie):
    cur = conn.cursor()
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur.execute("SELECT AVG(nombre_validation) AS nombre_validation, station_validation FROM validation WHERE date_tram BETWEEN %s AND %s AND sens = %s GROUP BY station_validation;", (start_date, end_date, selectedVoie,))
    result = cur.fetchall()
    data = [{"station_validation": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)


def get_moyPeriode_line(startDate, endDate, selectedVoie):
    cur = conn.cursor()
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur.execute("SELECT AVG(nombre_validation) AS nombre_validation, tranche_horaire FROM validation WHERE date_tram BETWEEN %s AND %s AND sens = %s GROUP BY tranche_horaire Order by tranche_horaire; ", (start_date, end_date, selectedVoie,))
    result = cur.fetchall()
    data = [{"tranche_horaire": row[1], "nombre_validation": row[0]} for row in result]
    return jsonify(data)