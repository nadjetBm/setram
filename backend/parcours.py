from flask import Flask, render_template, redirect, url_for, jsonify
from login import login
from rejeu import *
from flask import Flask, jsonify
from flask_cors import CORS
from database import conn
from datetime import timedelta
from datetime import datetime

def get_tramconducteur(date,selectedHour,selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT DISTINCT station FROM tramconducteur WHERE date_tram = '2022-05-02' AND SUBSTRING(station, 5) = %s", (selectedVoie))
    stations = [row[0] for row in cur.fetchall()]

    cur.execute("""SELECT heure,depart_theorique,depart_applicable,depart_reel,agenthabilite.nom_prenom,tramconducteur.matricule_agent,tram,etat,service_vehicule, station,retard_depart,avance_depart,temps_arret_theorique,temps_arret_applicable,temps_arret_reel
FROM tramconducteur
INNER JOIN agenthabilite ON tramconducteur.matricule_agent = agenthabilite.matricule_agent
WHERE date_tram = %s and tranche_horaire=%s ORDER BY heure """, (date,selectedHour))
    result = cur.fetchall()

    data = []

    for row in result:
        heure = row[0].strftime('%H:%M:%S')
        depart_theorique= row[1].strftime('%H:%M:%S')
        depart_applicable= row[2].strftime('%H:%M:%S')
        depart_reel= row[3].strftime('%H:%M:%S')
        nom_prenom=row[4]
        matricule_agent = row[5]
        tram = row[6]
        etat= row[7]
        service_vehicule= row[8]
        station = row[9]
        retard_depart = row[10].strftime('%H:%M:%S') if row[10] is not None else None
        avance_depart = row[11].strftime('%H:%M:%S') if row[11] is not None else None
        temps_arret_theorique= row[12].strftime('%H:%M:%S') if row[12] is not None else None
        temps_arret_applicable=row[13].strftime('%H:%M:%S') if row[13] is not None else None
        temps_arret_reel=row[14].strftime('%H:%M:%S') if row[14] is not None else None
        

        data.append({
            'heure': heure,
            'depart_theorique':depart_theorique,
            'depart_applicable': depart_applicable,
            'depart_reel':depart_reel,
            'nom_prenom':nom_prenom,
            'matricule_agent': matricule_agent,
            'tram': tram,
            'etat':etat,
            'service_vehicule':service_vehicule,
            'station':station,
            'retard_depart':retard_depart,
            'avance_depart':avance_depart,
            'temps_arret_theorique':temps_arret_theorique,
            'temps_arret_applicable':temps_arret_applicable,
            'temps_arret_reel':temps_arret_reel

        })

    response = {
        'stations': stations,
        'data': data
    }

    return jsonify(response)

def get_tramconducteurArrive(date,selectedHour,selectedVoie):
    cur = conn.cursor()
    cur.execute("SELECT DISTINCT station FROM tramconducteur WHERE date_tram = '2022-05-02' AND SUBSTRING(station, 5) = %s", (selectedVoie))
    stations = [row[0] for row in cur.fetchall()]

    cur.execute("""SELECT heure,arrive_theorique,arrive_applicable,arrive_reel,agenthabilite.nom_prenom,tramconducteur.matricule_agent,tram,etat,service_vehicule, station,retard_arrive,avance_arrive,temps_arret_theorique,temps_arret_applicable,temps_arret_reel
FROM tramconducteur
INNER JOIN agenthabilite ON tramconducteur.matricule_agent = agenthabilite.matricule_agent
WHERE date_tram = %s and tranche_horaire=%s ORDER BY heure""", (date,selectedHour))
    result = cur.fetchall()

    data = []

    for row in result:
        heure = row[0].strftime('%H:%M:%S')
        arrive_theorique= row[1].strftime('%H:%M:%S')
        arrive_applicable= row[2].strftime('%H:%M:%S')
        arrive_reel= row[3].strftime('%H:%M:%S')
        nom_prenom=row[4]
        matricule_agent = row[5]
        tram = row[6]
        etat= row[7]
        service_vehicule= row[8]
        station = row[9]
        retard_arrive = row[10].strftime('%H:%M:%S') if row[10] is not None else None
        avance_arrive = row[11].strftime('%H:%M:%S') if row[11] is not None else None
        temps_arret_theorique= row[12].strftime('%H:%M:%S') if row[12] is not None else None
        temps_arret_applicable=row[13].strftime('%H:%M:%S') if row[13] is not None else None
        temps_arret_reel=row[14].strftime('%H:%M:%S') if row[14] is not None else None
        

        data.append({
            'heure': heure,
            'arrive_theorique':arrive_theorique,
            'arrive_applicable': arrive_applicable,
            'arrive_reel':arrive_reel,
            'nom_prenom':nom_prenom,
            'matricule_agent': matricule_agent,
            'tram': tram,
            'etat':etat,
            'service_vehicule':service_vehicule,
            'station':station,
            'retard_arrive':retard_arrive,
            'avance_arrive':avance_arrive,
            'temps_arret_theorique':temps_arret_theorique,
            'temps_arret_applicable':temps_arret_applicable,
            'temps_arret_reel':temps_arret_reel

        })

    response = {
        'stations': stations,
        'data': data
    }

    return jsonify(response)