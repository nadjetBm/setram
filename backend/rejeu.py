from flask import Flask, jsonify, request
import folium
from folium.plugins import TimestampedGeoJson
from database import conn



app = Flask(__name__)

@app.route('/api/rejeu', methods=["POST"])
def rejeu():
    data = request.get_json()
    date = data.get('date')

    if not date:
        return jsonify({'error': 'missing date parameter'})
    # Créer un curseur pour exécuter des requêtes SQL
    cur = conn.cursor()
    # Exécuter une requête SQL pour récupérer les données nécessaires depuis la table tramconducteur
    cur.execute("SELECT date_tram, depart_reel, service_vehicule, tram, agenthabilite.nom_prenom, tramconducteur.matricule_agent, station, longitude, latitude, etat, retard_depart, retard_arrive, avance_depart, avance_arrive FROM tramconducteur INNER JOIN agenthabilite ON tramconducteur.matricule_agent = agenthabilite.matricule_agent WHERE date_tram = %s", (date,))

    # Récupérer les résultats de la requête
    results = cur.fetchall()

    # Créer une carte Folium
    m = folium.Map(location=[35.1978, -0.6359], zoom_start=14)


    # Ajouter un cercle rempli à la position cascade
    folium.Circle(location=[35.2089,-0.6162], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un marqueur à la position gres
    folium.Circle(location=[35.2131,-0.6158],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m) 
    
    # Ajouter un marqueur à la position AADN
    folium.Circle(location=[35.2167,-0.6148], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m) 
    
    # Ajouter un marqueur à la position Hamou
    folium.Circle(location=[35.2174,-0.6211],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m) 

    # Ajouter un autre marqueur à la position ENV
    folium.Circle(location=[35.2159,-0.6244], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)
  
    # Ajouter un autre marqueur à la position facu
    folium.Circle(location=[35.2152,-0.6327],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position niaa
    folium.Circle(location=[35.2167,-0.6385], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position compus
    folium.Circle(location=[35.2217,-0.6374], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

     # Ajouter un autre marqueur à la position nfge
    folium.Circle(location=[35.22745,-0.62690], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position gare nord
    folium.Circle(location=[35.22306,-0.62946], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

     # Ajouter un autre marqueur à la position AADL
    folium.Circle(location=[35.21829,-0.62753],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position sijilalai
    folium.Circle(location=[35.21584,-0.62353],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position wiam
    folium.Circle(location=[35.21132,-0.62792],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

     # Ajouter un autre marqueur à la position daira
    folium.Circle(location=[35.20592,-0.62640],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position cite
    folium.Circle(location=[35.19963,-0.62472],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position radio
    folium.Circle(location=[35.19624,-0.62080],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    
    # Ajouter un autre marqueur à la position maternité
    folium.Circle(location=[35.19241,-0.61914],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position adab
    folium.Circle(location=[35.19169,-0.62519],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)
   

    # Ajouter un autre marqueur à la position amir
    folium.Circle(location=[35.19114,-0.63012],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

    # Ajouter un autre marqueur à la position horloge
    folium.Circle(location=[35.19063,-0.63452], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)

     # Ajouter un autre marqueur à la position jardin
    folium.Circle(location=[35.18823,-0.64386],  radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)
  
    # Ajouter un autre marqueur à la position grsu
    folium.Circle(location=[35.18521,-0.65059], radius=30, color='darkblue', fill=True, fill_color='darkblue', fill_opacity=1.0).add_to(m)
  
    # Créer une ligne reliant les deux marqueurs
    line = folium.PolyLine(locations=[[35.2089,-0.6162],[35.2104,-0.6165],[35.2108,-0.6166], [35.2122,-0.6164],
    [35.2131,-0.6158], [35.2154,-0.6140], [35.2155,-0.6138], [35.2164,-0.6136], [35.2167,-0.6148], [35.21720,-0.61867],
    [35.21735,-0.62058],[35.2174,-0.6211], [35.21752,-0.62314], [35.21741,-0.62336], [35.21713,-0.62344],
    [35.21637,-0.62355], [35.21624,-0.62367],[35.21605,-0.62391], [35.2159,-0.6244], [35.21610,-0.62504],
    [35.21588,-0.62628], [35.2152,-0.6310], [35.2152,-0.6321], [35.2152,-0.6327], [35.2154,-0.6342],
    [35.2155,-0.6371],[35.2158,-0.6385],[35.2167,-0.6385],[35.2188,-0.6383],[35.2217,-0.6374],[35.2253,-0.6365],
    [35.2257,-0.6364], [35.2277,-0.6308], [35.22745,-0.62690], [35.22739,-0.62582],[35.22719,-0.62556],[35.22717,-0.62554], 
    [35.22698,-0.62549],[35.22547,-0.62581],[35.22487,-0.62588], [35.22476,-0.62597], [35.22455,-0.62629],
    [35.22447,-0.62749], [35.22438,-0.62847], [35.22417,-0.62916], [35.22383,-0.62946],[35.22353,-0.62950],[35.22306,-0.62946],
    [35.22225,-0.62946],[35.22148,-0.62943], [35.22025,-0.62906], [35.21859,-0.62856], [35.21829,-0.62753], 
    [35.21753,-0.62471],[35.21753,-0.62451],[35.21752,-0.62372],[35.21741,-0.62347], [35.21724,-0.62342],[35.21637,-0.62352],
    [35.21610,-0.62353], [35.21584,-0.62353], [35.21551,-0.62354], [35.21367,-0.62417],[35.21367,-0.62417], [35.21282,-0.62439],
    [35.21264,-0.62459],[35.21274,-0.62539], [35.21270,-0.62580], [35.21236,-0.62657], [35.21181,-0.62768],[35.21132,-0.62792],
    [35.21086,-0.62801],[35.21016,-0.62751], [35.20948,-0.62699], [35.20871,-0.62662],[35.20844,-0.62660], [35.20759,-0.62664], [35.20675,-0.62674], 
    [35.20640,-0.62674], [35.20592,-0.62640],[35.20487,-0.62575],[35.20472,-0.62566],[35.20444,-0.62572], [35.20380,-0.62622],[35.20089,-0.62610],
    [35.20074,-0.62601], [35.20051,-0.62581], [35.20021,-0.62536],[35.19963,-0.62472], [35.19673,-0.62139], [35.19624,-0.62080],
    [35.19446,-0.61881],[35.19425,-0.61874], [35.19279,-0.61838], [35.19251,-0.61856],[35.19241,-0.61914],[35.19223,-0.62036],
    [35.19201,-0.62256], [35.19179,-0.62440],[35.19169,-0.62519],[35.19138,-0.62821],[35.19128,-0.62873], [35.19115,-0.62972],
    [35.19114,-0.63012],[35.19103,-0.63109], [35.19091,-0.63229], [35.19077,-0.63348],[35.19063,-0.63452],
    [35.19045,-0.63582],[35.19040,-0.63647],[35.19004,-0.63928],[35.18992,-0.63986],[35.18975,-0.64034], [35.18823,-0.64386],
    [35.18521,-0.65059]], weight=5, color='darkblue').add_to(m)


    # Créer une liste pour stocker les données des trams pour la timeline
    tram_data = []
    stationMapping = {
        'JARD1': 'jardin public <br/> <b>Voie: </b> 1',
        'AADL1': 'AADL <br/> <b>Voie: </b> 1',
        'FACU1': 'Faculté de Droit <br/> <b>Voie: </b> 1',
        'SIDJ1': 'Sidi Djillali<br/> <b>Voie: </b> 1',
        'WIAM1': 'Place El Wiam<br/> <b>Voie: </b> 1',
        'MATE1': 'Maternité<br/> <b>Voie: </b> 1',
        'HAMO1': 'Ben Hamouda<br/> <b>Voie: </b> 1',
        'NGFE1': 'Gare Ferroviaire<br/> <b>Voie: </b> 1',
        'RADI1': 'Radio<br/> <b>Voie: </b> 1',
        'ADNA1': 'Les Frères Adnane<br/> <b>Voie: </b> 1',
        'NIAA1': 'Centre En Niaama<br/> <b>Voie: </b> 1',
        'HORL1': 'Quatre Horloges<br/> <b>Voie: </b> 1',
        'GRES1': 'Gare Routière Est<br/> <b>Voie: </b> 1',
        'CASC1': 'Les Cascades<br/> <b>Voie: </b> 1',
        'GRSU1': 'Gare Routière Sud<br/> <b>Voie: </b> 1',
        'CAMP1': 'Campus Universitaire<br/> <b>Voie: </b> 1',
        'GRNO1': 'Gare Routière Nord<br/> <b>Voie: </b> 1',
        'CITE1': 'Cité H. Boumedienne<br/> <b>Voie: </b> 1',
        'ADAB1': 'Adda Boudjellal <br/> <b>Voie: </b> 1',
        'ENVI1': 'Environnement <br/> <b>Voie: </b> 1',
        'EABD1': 'Emir Abdelkader<br/> <b>Voie: </b> 1',
        'DAIR1': 'La Daira<br/> <b>Voie: </b> 1',
        'JARD2': 'jardin public <br/> <b>Voie: </b> 2',
        'FACU2': 'Faculté de Droit <br/> <b>Voie: </b> 2',
        'SIDJ2': 'Sidi Djillali <br/> <b>Voie: </b> 2',
        'WIAM2': 'Place El Wiam <br/> <b>Voie: </b> 2',
        'MATE2': 'Maternité <br/> <b>Voie: </b> 2',
        'HAMO2': 'Ben Hamouda <br/> <b>Voie: </b> 2',
        'NGFE2': 'Gare Ferroviaire <br/> <b>Voie: </b> 2',
        'RADI2': 'Radio  <br/> <b>Voie: </b> 2',
        'ADNA2': 'Les Frères Adnane  <br/> <b>Voie: </b> 2',
        'NIAA2': 'Centre En Niaama <br/> <b>Voie: </b> 2',
        'HORL2': 'Quatre Horloges  <br/> <b>Voie: </b> 2',
        'GRES2': 'Gare Routière Est <br/> <b>Voie: </b> 2',
        'CASC2': 'Les Cascades <br/> <b>Voie: </b> 2',
        'GRSU2': 'Gare Routière Sud <br/> <b>Voie: </b> 2',
        'CAMP2': 'Campus Universitaire <br/> <b>Voie: </b> 2',
        'GRNO2': 'Gare Routière Nord <br/> <b>Voie: </b> 2',
        'CITE2': 'Cité H. Boumedienne <br/> <b>Voie: </b> 2',
        'ADAB2': 'Adda Boudjellal <br/> <b>Voie: </b> 2',
        'ENVI2': 'Environnement <br/> <b>Voie: </b> 2',
        'EABD2': 'Emir Abdelkader <br/> <b>Voie: </b> 2',
        'DAIR2': 'La Daira <br/> <b>Voie: </b> 2',
        'AADL2': 'AADL <br/> <b>Voie: </b> 2'
  }
    etatMapping = {
        'COM': 'Commercial',
        'HLP': 'Sans voyageurs',
    }
   
 # Boucler à travers les résultats pour ajouter les données des trams à la liste pour la timeline
    for row in results:
        date_tram, depart_reel, service_vehicule, tram, nom_prenom, matricule_agent, station, longitude, latitude, etat, retard_depart, retard_arrive, avance_depart, avance_arrive = row
        if latitude is not None and longitude is not None:
            if avance_depart is None:
                avance_depart = "00:00:00"
            if retard_depart is None:
                retard_depart = "00:00:00"
            if avance_arrive is None:
                avance_arrive = "00:00:00"
            if retard_arrive is None:
                retard_arrive = "00:00:00"

            station_name = stationMapping.get(station, 'DEPO')
            etat_name = etatMapping.get(etat, '/')

            tram_data.append({
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [longitude, latitude]
                },
                'properties': {
                    'time': date_tram.strftime('%Y-%m-%d') + ' ' + depart_reel.strftime('%H:%M:%S'),
                    'popup': f"<b>Date :</b> {date_tram}<br/><b>Matricule du conducteur :</b> {matricule_agent}<br/><b>Nom du conducteur :</b> {nom_prenom}<br/><b>Service véhicule :</b> {service_vehicule}<br/><b>Tram :</b> {tram}<br/><b>Etat :</b> {etat_name}<br/><b>Retard à l'arrivée :</b> {retard_arrive}<br/><b>Avance à l'arrivée :</b> {avance_arrive}<br/><b>Retard au départ :</b> {retard_depart}<br/><b>Avance au départ :</b> {avance_depart}<br/><b>Station :</b> {station_name}<br/>",
                  
                }
            })

           

   

    # Créer un GeoJson avec la liste des données des trams pour la timeline
    tram_geojson = {
        'type': 'FeatureCollection',
        'features': tram_data
    }

    # Ajouter la timeline à la carte
    TimestampedGeoJson(tram_geojson, period='PT1M', duration='PT1M', add_last_point=True, auto_play=False).add_to(m)

    # Sauvegarder la carte dans un fichier HTML
    m.save("../frontend/public/rejeu.html")

    # Fermer le curseur
    cur.close()

    # Retourner un message JSON pour indiquer que la génération de la carte a été effectuée avec succès
    return jsonify({'result': 'success'})


if __name__ == '__main__':
    app.run(debug=True)
