from flask import Flask, render_template, redirect, url_for, jsonify
from flask import Flask, request, session, jsonify
from database import conn

app = Flask(__name__)
def profile():
    if 'logged_in' in session:
        role_compte = session['role_compte']
        nom_prenom = session['nom_prenom']

        cursor = conn.cursor()
        cursor.execute("SELECT * FROM comptes WHERE role_compte=%s and nom_prenom=%s", (role_compte, nom_prenom,))
        compte = cursor.fetchone()

        if compte:
            compte_data = {
                'nom_prenom': compte[0],
                'role_compte': compte[3],
                'mot_de_passe': compte[2] 
            }
            return jsonify(compte_data)
        else:
            print("Aucun compte trouvé pour le nom d'utilisateur:", nom_prenom, role_compte)
    else:
        print("Utilisateur non connecté")

    return jsonify({'message': 'Erreur lors de la récupération du profil'})

def modifier_motDePasse(password, newPassword):
    cur = conn.cursor()
    role_query = "UPDATE comptes SET mot_de_passe = %s WHERE mot_de_passe = %s"
    cur.execute(role_query, (newPassword, password))
    conn.commit()
    cur.close()
    return jsonify({'message': 'Le mot de passe a été modifié avec succès!'})

