from flask import Flask, render_template, redirect, url_for, jsonify
from login import login
from rejeu import *
from flask import Flask, jsonify
from flask_cors import CORS
from database import conn
from datetime import timedelta
from datetime import datetime

app = Flask(__name__)
######################################## Alarme jour######
def get_jourtramAlarme(date):
    cur = conn.cursor()
    cur.execute("""WITH subquery AS (
    SELECT
        tram_alarme,
        COUNT(DISTINCT CASE WHEN type_alarme = 'Freinage d''urgence' THEN EXTRACT(HOUR FROM heure_debut_alarme) ELSE 0 END) AS alarmeU_count,
        COUNT(DISTINCT matricule_agent) AS agent_count,
    SUM(CASE WHEN notification.type_alarme = 'Activation poignées de déverrouillage d''urgence' THEN 1 ELSE 0 END) AS alarmeA,
    SUM(CASE WHEN notification.type_alarme = 'Freinage de sécurité' THEN 1 ELSE 0 END) AS alarmeS
    FROM notification
    WHERE date_notification = %s AND tram_alarme IS NOT NULL
    GROUP BY tram_alarme
)
SELECT
    tram_alarme,
    alarmeU_count - agent_count AS nb_alarmeU, 
  alarmeA, 
  alarmeS  
FROM subquery;
    """, (date,))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"tram_alarme": row[0]}
        data_dict["Activation poignées de déverrouillage d'urgence"] = row[2]
        data_dict["Freinage de sécurité"] = row[3]
        data_dict["Freinage d'urgence"] = row[1]
        data.append(data_dict)

    return jsonify(data)

########
def get_jourHoraireAlarme(date):
    cur = conn.cursor()
    cur.execute("""SELECT
    EXTRACT(HOUR FROM heure_debut_alarme) AS tranche_horaire,
    COUNT(DISTINCT CASE WHEN type_alarme = 'Freinage d''urgence' THEN 1 ELSE 0 END) AS alarmeU_count,
    SUM(CASE WHEN type_alarme = 'Activation poignées de déverrouillage d''urgence' THEN 1 ELSE 0 END) AS alarmeA,
    SUM(CASE WHEN type_alarme = 'Freinage de sécurité' THEN 1 ELSE 0 END) AS alarmeS
FROM notification
WHERE date_notification = %s AND tram_alarme IS NOT NULL
GROUP BY EXTRACT(HOUR FROM heure_debut_alarme);""", (date,))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"tranche_horaire": row[0]}
        data_dict["Activation poignées de déverrouillage d'urgence"] = row[2]
        data_dict["Freinage de sécurité"] = row[3]
        data_dict["Freinage d'urgence"] = row[1]
        data.append(data_dict)

    return jsonify(data)
#########Alarme periode####
@app.route('/periodeTramAlarme/<startDate>/<endDate>')
def get_periodeTramAlarme(startDate, endDate):
    cur = conn.cursor()
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur.execute("""WITH subquery AS (
    SELECT
        tram_alarme,
        COUNT(DISTINCT CASE WHEN type_alarme = 'Freinage d''urgence' THEN EXTRACT(HOUR FROM heure_debut_alarme) END) AS alarmeU_count,
        COUNT(DISTINCT matricule_agent) AS agent_count,
    SUM(CASE WHEN notification.type_alarme = 'Activation poignées de déverrouillage d''urgence' THEN 1 ELSE 0 END) AS alarmeA,
    SUM(CASE WHEN notification.type_alarme = 'Freinage de sécurité' THEN 1 ELSE 0 END) AS alarmeS
    FROM notification
    WHERE date_notification BETWEEN %s AND %s  AND tram_alarme IS NOT NULL
    GROUP BY tram_alarme
)
SELECT
    tram_alarme,
    alarmeU_count - agent_count AS nb_alarmeU, 
  alarmeA, 
  alarmeS  
FROM subquery; """, (start_date, end_date))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"tram_alarme": row[0]}
        data_dict["Activation poignées de déverrouillage d'urgence"] = row[2]
        data_dict["Freinage de sécurité"] = row[3]
        data_dict["Freinage d'urgence"] = row[2]
        data.append(data_dict)

    return jsonify(data)
######
def get_periodeHoraireAlarme(startDate, endDate):
    # Convert date strings to datetime objects
    start_date = datetime.strptime(startDate, "%a %b %d %Y %H:%M:%S GMT%z").date()
    end_date = datetime.strptime(endDate.strip('}'), "%a %b %d %Y %H:%M:%S GMT%z").date()

    cur = conn.cursor()
    cur.execute("""
        SELECT 
            SUM(CASE WHEN dim_alarme.type_alarme = 'Activation poignées de déverrouillage d''urgence' THEN 1 ELSE 0 END), 
            SUM(CASE WHEN dim_alarme.type_alarme = 'Freinage de sécurité' THEN 1 ELSE 0 END), 
            SUM(CASE WHEN dim_alarme.type_alarme = 'Freinage d''urgence' THEN 1 ELSE 0 END) / 10, 
            tranche_horaire 
        FROM dim_alarme 
        WHERE date_alarme BETWEEN %s AND %s 
        GROUP BY tranche_horaire
        ORDER BY tranche_horaire;
    """, (start_date, end_date))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {
            "tranche_horaire": row[3],
            "Activation poignées de déverrouillage d'urgence": row[0],
            "Freinage de sécurité": row[1],
            "Freinage d'urgence": row[2]
        }
        data.append(data_dict)

    return jsonify(data)
##########"Conducteur Jour"
def get_jourConducteur(date):
    cur = conn.cursor()
    cur.execute("SELECT COUNT(DISTINCT nom_prenom) AS habilitation, nom_prenom, duree_conduite FROM t_fait_conduite WHERE date_conduite = %s GROUP BY duree_conduite, nom_prenom;", (date,))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"habilitation": row[0]}
        data_dict["nom_prenom"] = row[1]
        data_dict["duree_conduite"] = row[2].strftime("%H:%M:%S")
        data.append(data_dict)

    return jsonify(data)
###################Conducteur Periode 
def get_periodeConducteur(startDate,endDate):
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur = conn.cursor()
    cur.execute("SELECT SUM(habilitation) AS habilitation, nom_prenom, SUM(duree_conduite) AS duree_conduite FROM ( SELECT COUNT(DISTINCT nom_prenom) AS habilitation, nom_prenom, duree_conduite FROM t_fait_conduite WHERE date_conduite BETWEEN %s AND %s GROUP BY date_conduite, matricule_agent, nom_prenom, duree_conduite) AS subquery GROUP BY nom_prenom;", (start_date, end_date))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"habilitation": row[0]}
        data_dict["nom_prenom"] = row[1]
        total_seconds = row[2].total_seconds()
        hours = int(total_seconds // 3600)
        minutes = int((total_seconds % 3600) // 60)
        seconds = int(total_seconds % 60)

        duration_str = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
        data_dict["duree_conduite"] = duration_str
        data.append(data_dict)

    return jsonify(data)
######################### tram km jour######
def get_jourTram(date):
    cur = conn.cursor()
    cur.execute("SELECT DISTINCT(numero_parc) AS tram, km_parcourus_com AS com , km_parcourus_hlp AS hlp From dim_tram WHERE date_tram= %s ", (date,))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"tram": row[0]}
        data_dict["com"] = round(row[1], 2) if row[1] is not None else None
        data_dict["hlp"] = round(row[2], 2) if row[2] is not None else None
        data.append(data_dict)

    return jsonify(data)
############### tram km  periode######
@app.route('/periodeTram/<startDate>/<endDate>')
def get_periodeTram(startDate,endDate):
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur = conn.cursor()
    cur.execute("SELECT SUM(km_parcourus_com) AS com,SUM(km_parcourus_hlp) AS hlp,numero_parc FROM ( SELECT DISTINCT (numero_parc), km_parcourus_com, km_parcourus_hlp FROM dim_tram WHERE date_tram BETWEEN %s and %s GROUP BY date_tram, numero_parc,km_parcourus_com, km_parcourus_hlp) AS subquery GROUP BY numero_parc; ", (startDate,endDate,))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"tram": row[2]}
        data_dict["com"] = round(row[0], 2) if row[0] is not None else None
        data_dict["hlp"] = round(row[1], 2) if row[1] is not None else None
        data.append(data_dict)

    return jsonify(data)

############### tram  course date ######
@app.route('/tram-data/<date>')
def get_tram_data(date):
    cur = conn.cursor()
    cur.execute("select DISTINCT(dim_tram.numero_parc) AS tram, t_fait_pcc.duree_course AS duree_course from t_fait_pcc, dim_tram, dim_service_vehicule where t_fait_pcc.id_dim_sv = dim_service_vehicule.id_tab_dim_service_vehicule and dim_service_vehicule.id_tab_dim_tram = dim_tram.id_tab_dim_tram and dim_tram.date_tram = %s", (date,))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"tram": row[0]}
        duree_course = row[1]
        seconds = duree_course.hour * 3600 + duree_course.minute * 60 + duree_course.second
        data_dict["duree_course"] = seconds
        data.append(data_dict)

    return jsonify(data)

############### tram  course periode######
@app.route('/periode-tram-data/<startDate>/<endDate>')
def get_periode_tram_data(startDate, endDate):
    start_date = datetime.strptime(startDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    end_date = datetime.strptime(endDate, '%a %b %d %Y %H:%M:%S GMT%z').date()
    cur = conn.cursor()
    cur.execute("SELECT subquery.numero_parc,SUM(subquery.duree_course) AS duree_course FROM (SELECT DISTINCT dim_tram.numero_parc, t_fait_pcc.duree_course FROM dim_tram, t_fait_pcc, dim_service_vehicule WHERE t_fait_pcc.id_dim_sv = dim_service_vehicule.id_tab_dim_service_vehicule AND dim_service_vehicule.id_tab_dim_tram = dim_tram.id_tab_dim_tram AND date_tram BETWEEN %s AND %s GROUP BY date_tram, dim_tram.numero_parc, t_fait_pcc.duree_course) AS subquery GROUP BY subquery.numero_parc;", (start_date, end_date))
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"tram": row[0]}
        duree_course = row[1]
        seconds = duree_course.total_seconds()
        hours = seconds // 3600
        minutes = (seconds % 3600) // 60
        seconds = seconds % 60
        data_dict["duree_course"] = {
            "hours": int(hours),
            "minutes": int(minutes),
            "seconds": int(seconds)
        }
        data.append(data_dict)

    conn.commit()  # Valider la transaction


    return jsonify(data)
