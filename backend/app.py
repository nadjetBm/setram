from flask import Flask, render_template, redirect,session, url_for, jsonify
from login import *
from profil import *
from rejeu import *
from role import *
from parcours import *
from setramien import *
from frequentation import *
from suivi import *
from exploitation import *
from flask import Flask, jsonify
from flask_cors import CORS
from database import conn
from datetime import datetime
from datetime import timedelta







app = Flask(__name__)

CORS(app)
app.secret_key = 'votre_clé_secrète'
@app.route('/login', methods=['POST'])
def login_route():
    return login()

@app.route('/logout', methods=['POST'])
def logout():
    session.clear()
    return jsonify({'message': 'Déconnexion réussie'})

########profile#####
@app.route('/profile', methods=['GET'])
def profil_route():
    return profile()

@app.route('/modifierMotDePasse/<password>/<newPassword>', methods=['PUT'])
def modifier_motDePasse_route(password, newPassword):
    return modifier_motDePasse(password, newPassword)
   
@app.route('/modifier_compte/<nomPrenom>', methods=['PUT'])
def modifierCompte_route(nomPrenom):
    return modifier_compte(nomPrenom)
#####Role######
@app.route('/modifier_conducteur/<idAgent>', methods=['PUT'])
def modifierConducteur_route(idAgent):
    return modifier_conducteur(idAgent)
#####Role######

@app.route('/create_role', methods=['POST'])
def createRole_route():
    return create_role()



@app.route('/modifier_role/<roleName>', methods=['PUT'])
def modifierRole_route(roleName):
    return modifier_role(roleName)

@app.route('/delete_role/<role_name>', methods=['DELETE'])
def delete_role_route(role_name):
    return delete_role(role_name)

@app.route('/display_components', methods=['GET'])
def display_components_route():
    return display_components()

@app.route('/role')
def get_role_route():
    return get_role()


########Setramien#######
@app.route('/afficherRole', methods=['GET'])
def get_afficherRole_route():
    return get_afficherRole()

@app.route('/AjouterAgent', methods=['POST'])
def AjouterAgent_route():
    return AjouterAgent()

@app.route('/AjouterConducteur', methods=['POST'])
def AjouterConducteur_route():
    return AjouterConducteur()

@app.route('/conducteur')
def get_conducteur_route():
     return get_conducteur()

@app.route('/utilisateur')
def get_utilisateur_route():
    return get_utilisateur()
 
 ########Rejeu##########
@app.route('/api/rejeu', methods=['POST'])
def rejeu_route():
    return rejeu()
#################frequentation###################
@app.route('/progress-bar-data/<date>/<selectedVoie>')
def get_progress_bar_data_route(date, selectedVoie):
    return get_progress_bar_data(date, selectedVoie)

@app.route('/validation-data/<date>/<selectedVoie>')
def get_validation_data_route(date, selectedVoie):
    return get_validation_data(date, selectedVoie)

@app.route('/tranche-data/<date>/<selectedVoie>')
def get_horaire_data_route(date, selectedVoie):
    return get_horaire_data(date, selectedVoie)

@app.route('/moy-bar/<date>/<selectedVoie>')
def get_moybar_route(date, selectedVoie):
    return get_moybar(date, selectedVoie)

@app.route('/station-data/<date>/<selectedVoie>')
def get_station_data_route(date, selectedVoie):
    return get_station_data(date, selectedVoie)


@app.route('/moy-tranche-data/<date>/<selectedVoie>')
def get_moy_horaire_data_route(date, selectedVoie):
    return get_moy_horaire_data(date, selectedVoie)

@app.route('/nbPeriode-progress/<startDate>/<endDate>/<selectedVoie>')
def get_nbPeriode_progress_route(startDate, endDate, selectedVoie):
    return get_nbPeriode_progress(startDate, endDate, selectedVoie)

@app.route('/nbPeriode-bar/<startDate>/<endDate>/<selectedVoie>')
def get_nbPeriode_bar_route(startDate, endDate, selectedVoie):
    return get_nbPeriode_bar(startDate, endDate, selectedVoie)



@app.route('/nbPeriode-line/<startDate>/<endDate>/<selectedVoie>')
def get_nbPeriode_line_route(startDate, endDate, selectedVoie):
    return get_nbPeriode_line(startDate, endDate)

@app.route('/moyPeriode-progress/<startDate>/<endDate>/<selectedVoie>')
def get_moyPeriode_progress_route(startDate, endDate, selectedVoie):
    return get_moyPeriode_progress(startDate, endDate, selectedVoie) 


@app.route('/moyPeriode-bar/<startDate>/<endDate>/<selectedVoie>')
def get_moyPeriode_bar_route(startDate, endDate, selectedVoie):
    return get_moyPeriode_bar(startDate, endDate)

@app.route('/moyPeriode-line/<startDate>/<endDate>/<selectedVoie>')
def get_moyPeriode_line_route(startDate, endDate, selectedVoie):
    return get_moyPeriode_line(startDate, endDate, selectedVoie)
##########Suivi ######
#Alarme jour###############
@app.route('/jourtramAlarme/<date>')
def get_jourtramAlarme_route(date):
    return get_jourtramAlarme(date)


@app.route('/jourHoraireAlarme/<date>')
def get_jourHoraireAlarme_route(date):
    return get_jourHoraireAlarme(date)
    
#Alarme periode ######################
@app.route('/periodeTramAlarme/<startDate>/<endDate>')
def get_periodeTramAlarme_route(startDate, endDate):
    return get_periodeTramAlarme(startDate, endDate)


@app.route('/periodeHoraireAlarme/<startDate>/<endDate>')
def get_periodeHoraireAlarme_route(startDate, endDate):
    return get_periodeHoraireAlarme(startDate, endDate)
  
#################conducteur jour########
@app.route('/jourConducteur/<date>')
def get_jourConducteur_route(date):
   return get_jourConducteur(date)

#################conducteur periode########
@app.route('/periodeConducteur/<startDate>/<endDate>')
def get_periodeConducteur_route(startDate,endDate):
    return get_periodeConducteur(startDate,endDate)
   

######################### tram km jour######
@app.route('/jourTram/<date>')
def get_jourTram_route(date):
    return get_jourTram(date)

############### tram km periode ######
@app.route('/periodeTram/<startDate>/<endDate>')
def get_periodeTram_route(startDate,endDate):
    return get_periodeTram(startDate,endDate)

#########tram course date ###########""
@app.route('/tram-data/<date>')
def get_tram_data_route(date):
     return get_tram_data(date)

#########tram course periode ###########""
    
@app.route('/periode-tram-data/<startDate>/<endDate>')
def get_periode_tram_data_route(startDate, endDate):
     return get_periode_tram_data(startDate, endDate)


#############Exploitation###########   
####### vitesse co pour chaque tram################
@app.route('/vitesse-tram-data/<date>')
def get_vitesse_tram_data_route(date):
    return get_vitesse_tram_data(date)
    

################# vitess com###########
@app.route('/vitesse-data/<date>')
def get_vitesse_data_route(date):
    return get_vitesse_data(date)
################# vitess com PERIODE ###########""
@app.route('/vitessePeriode-data/<startDate>/<endDate>')
def get_vitesse_periode_data_route(startDate,endDate):
    return get_vitesse_periode_data(startDate,endDate)
    
# #################### vitesse co pour chaque tram periode ###############
@app.route('/vitessePeriode-tram-data/<startDate>/<endDate>')
def get_vitesse_periode_tram_data_route(startDate,endDate):
    return get_vitesse_periode_tram_data(startDate,endDate)

#################Parcours ###########
@app.route('/parcours-data/<date>')
def get_parcours_data_route(date):
    return get_parcours_data(date)

###########################################
@app.route('/parcoursTram-data/<date>')
def get_parcoursTram_data_route(date):
    return get_parcoursTram_data(date)

############################Periode Parcous###########

@app.route('/parcoursPeriode-data/<startDate>/<endDate>')
def get_parcoursPeriode_data_route(startDate, endDate):
    return get_parcoursPeriode_data(startDate, endDate)
    
##################parcoursTramPeriode##############
@app.route('/parcoursTramPeriode-data/<startDate>/<endDate>')
def get_parcoursTramPeriode_data_route(startDate, endDate):
    return get_parcoursTramPeriode_data(startDate, endDate)
   
################ Arret Jour ###############################################
@app.route('/arret-data/<date>')
def get_arret_data_route(date):
    return get_arret_data(date)
  
################################################################
@app.route('/arretStation-data/<date>')
def get_arretStation_data_route(date):
    return get_arretStation_data(date)
   
#################################################################
@app.route('/arretHoraire-data/<date>')
def get_arretHoraire_data_route(date):
   return get_arretHoraire_data(date)

################ Arret Periode ###############################################
@app.route('/arretPeriode-data/<startDate>/<endDate>')
def get_arretPeriode_data_route(startDate, endDate):
    return get_arretPeriode_data(startDate, endDate)
    
################################################################
@app.route('/arretStationPeriode-data/<startDate>/<endDate>')
def get_arretStationPeriode_data_route(startDate, endDate):
    return get_arretStationPeriode_data(startDate, endDate)
########################
@app.route('/arretHorairePeriode-data/<startDate>/<endDate>')
def get_arretHorairePeriode_data_route(startDate, endDate):
    return get_arretHorairePeriode_data(startDate, endDate)
   
################################ Arreive Jour ###############################################
@app.route('/retardArrive-data/<date>')
def get_retardArrive_data_route(date):
    return get_retardArrive_data(date)


#########################
@app.route('/retardArriveStation-data/<date>')
def get_retardArriveStation_data_route(date):
    return get_retardArriveStation_data(date)
################################ retard depart Jour ###############################################
@app.route('/retardDepart-data/<date>')
def get_retardDepart_data_route(date):
    return get_retardDepart_data(date)

#########################
@app.route('/retardDepartStation-data/<date>')
def get_retardDepartStation_data_route(date):
    return get_retardDepartStation_data(date)

###########################################retard arrive periode 
@app.route('/retardArrivePeriode-data/<startDate>/<endDate>')
def get_retardArrivePeriode_data_route(startDate, endDate):
    return get_retardArrivePeriode_data(startDate, endDate)
    

########################
@app.route('/retardArriveStationPeriode-data/<startDate>/<endDate>')
def get_retardArriveStationPeriode_data_route(startDate, endDate):
    return get_retardArriveStationPeriode_data(startDate, endDate)
    
########periode########
@app.route('/retardDepartPeriode-data/<startDate>/<endDate>')
def get_retardDepartPeriode_data_route(startDate, endDate):
    return get_retardDepartPeriode_data(startDate, endDate)
 

##########avanceRetard#######################
@app.route('/retardDepartStationPeriode-data/<startDate>/<endDate>')
def get_retardDepartStationPeriode_data_route(startDate, endDate):
   return  get_retardDepartStationPeriode_data(startDate, endDate)


@app.route('/tramconducteur/<date>/<selectedHour>/<selectedVoie>')
def get_tramconducteur_route(date,selectedHour,selectedVoie):
    return get_tramconducteur(date,selectedHour,selectedVoie)

@app.route('/tramconducteurArrive/<date>/<selectedHour>/<selectedVoie>')
def get_tramconducteurArrive_route(date,selectedHour,selectedVoie):
    return get_tramconducteurArrive(date,selectedHour,selectedVoie)
    











@app.route('/')
def index():
    return 'Welcome to my API!'











if __name__ == '__main__':
    app.run(debug=True)
