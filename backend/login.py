from flask import Flask, request, session, jsonify
from database import conn

app = Flask(__name__)
app.secret_key = 'votre_clé_secrète'
def login():
    nom_prenom = request.json['nom_prenom']
    mot_de_passe = request.json['mot_de_passe']
    
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM comptes WHERE nom_prenom=%s AND mot_de_passe=%s", (nom_prenom, mot_de_passe))
    compte = cursor.fetchone()
    
    if compte:
        column_names = [desc[0] for desc in cursor.description]
        compte_dict = dict(zip(column_names, compte))
        
        if compte_dict['compte']:  # Vérification si le compte est activé
            session['logged_in'] = True
            session['nom_prenom'] = compte_dict['nom_prenom']
            session['role_compte'] = compte_dict['role_compte']
            session['mot_de_passe'] = compte_dict['mot_de_passe']

            if compte_dict['role_compte'] == 'Administrateur':
                return jsonify({'redirect': '/dashboard'})
            else:
                return jsonify({'redirect': '/dashboardUser'})
        else:
            return jsonify({'message': 'Le compte est désactivé'})
    else:
        return jsonify({'message': 'Informations invalides'})