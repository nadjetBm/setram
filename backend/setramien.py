from flask import Flask, render_template, redirect, url_for, jsonify
from login import login
from rejeu import *
from flask import Flask, jsonify
from flask_cors import CORS
from database import conn
from datetime import timedelta
from datetime import datetime
################################""
def get_conducteur():
    cur = conn.cursor()
    cur.execute("SELECT id_tab_agent_habilite , nom_prenom, matricule_agent FROM agenthabilite")
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"id_tab_agent_habilite": row[0]}
        data_dict["nom_prenom"] = row[1]
        data_dict["matricule_agent"] = row[2]
        data.append(data_dict)

    return jsonify(data)
########
@app.route('/AjouterAgent', methods=['POST'])
def AjouterAgent():
    data = request.get_json()
    nom_prenom = data.get('nom_prenom')
    email = data.get('email')
    matricule = data.get('matricule')
    role_compte = data.get('role_compte')

    cur = conn.cursor()
    cur.execute("SELECT * FROM agenthabilite WHERE matricule_agent=%s or email=%s", (matricule,email))
    user = cur.fetchone()
    if user:
        # Si l'utilisateur existe déjà, retournez un message d'erreur
        return jsonify({'success': False, 'message': 'Utilisateur avec ce matricule ou cet e-mail existe déjà'})
   
     # Check if email ends with "@setram-sba.dz"
    elif not email.endswith("@setram-sba.dz"):
        return jsonify({'success': False})
    
    elif not (len(matricule) == 5 and matricule.isdigit()):
        return jsonify({'success': False})

    if role_compte in ['Agent habilité', 'Conducteur']:
        # Insérez le nouvel utilisateur dans la table agenthabilite
        cur.execute("INSERT INTO agenthabilite(nom_prenom, email, matricule_agent, role_agent_habilite) VALUES (%s, %s, %s, %s)",
                    (nom_prenom, email, matricule, role_compte))
        conn.commit()
    else:
        # Insérez le nouvel utilisateur dans la table agenthabilite
        cur.execute("INSERT INTO agenthabilite(nom_prenom, email, matricule_agent, role_agent_habilite) VALUES (%s, %s, %s, %s)",
                    (nom_prenom, email, matricule, role_compte))
        conn.commit()

        # Insérez le nouvel utilisateur dans la table comptes
        cur.execute("INSERT INTO comptes(nom_prenom, email, mot_de_passe, role_compte) VALUES (%s, %s, %s, %s)",
                    (nom_prenom, email, matricule, role_compte))
        conn.commit()

    # Retournez un message de succès et les données utilisateur pertinentes
    user_data = {'email': email}  # Remplacez par vos propres données utilisateur
    return jsonify({'success': True, 'message': 'Enregistrement réussi', 'user': user_data})

############Afficher les role pour la page ajouter ########
def get_afficherRole():
    cur = conn.cursor()
    cur.execute("SELECT role_name FROM role")
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"role_name": row[0]}
        
        data.append(data_dict)

    return jsonify(data)
###########
def get_utilisateur():
    cur = conn.cursor()
    cur.execute("SELECT nom_prenom, email , mot_de_passe AS matricule, role_compte,compte FROM comptes")
    result = cur.fetchall()

    data = []
    for row in result:
        data_dict = {"nom_prenom": row[0]}
        data_dict["email"] = row[1]
        data_dict["matricule"] = row[2]
        data_dict["role_compte"] = row[3]
        data_dict["compte"] = row[4]
        data.append(data_dict)

    return jsonify(data)
##########
def modifier_compte(nomPrenom):
    cur = conn.cursor()
     # Check if email ends with "@setram-sba.dz"
    
    role_query = "UPDATE comptes SET email = %s, mot_de_passe = %s, role_compte = %s, compte = %s WHERE nom_prenom = %s"
    cur.execute(role_query, (
      
        request.json['email'],
        request.json['matricule'],
        request.json['role'],
        request.json['compte'],
        nomPrenom
    ))
    conn.commit()
    cur.close()
    return jsonify({'message': 'Le rôle a été modifié avec succès!'})

##########
def modifier_conducteur(idAgent):
    cur = conn.cursor()
     # Check if email ends with "@setram-sba.dz"
    
    role_query = "UPDATE agenthabilite SET matricule_agent = %s,nom_prenom = %s WHERE id_tab_agent_habilite = %s"
    cur.execute(role_query, (    
        request.json['matricule_agent'],
        request.json['nom_prenom'],
        idAgent
    ))
    conn.commit()
    cur.close()
    return jsonify({'message': 'Les informations du conducteurs ont été modifiées avec succès!'})

########
@app.route('/AjouterConducteur', methods=['POST'])
def AjouterConducteur():
    data = request.get_json()
    nom_prenom = data.get('nom_prenom')
    matricule = data.get('matricule')
   
    cur = conn.cursor()
    cur.execute("SELECT * FROM agenthabilite WHERE matricule_agent=%s or nom_prenom=%s", (matricule,nom_prenom))
    user = cur.fetchone()
    if user:
        # Si l'utilisateur existe déjà, retournez un message d'erreur
        return jsonify({'success': False, 'message': 'Conducteur existe déjà'})
   
   
    
    elif not (len(matricule) == 5 and matricule.isdigit()):
        return jsonify({'success': False})

    cur.execute("INSERT INTO agenthabilite(nom_prenom, matricule_agent) VALUES ( %s, %s)",
                    (nom_prenom,matricule))
    conn.commit()

    return jsonify({'success': True, 'message': 'Enregistrement réussi'})
