import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FaUser, FaRedo,FaUserCheck, FaHourglassStart, FaTram,FaChartBar, FaMoon, FaSun, FaSignOutAlt} from 'react-icons/fa';
import './SideBar.css';
import Logo from '../../assets/logo_sideBar.png';
import { Link } from 'react-router-dom';

function SideBar(props) {
  const [rejeu, setRejeu] = useState(false);
  const [frequentation, setFrequentation] = useState(false);
  const [exploitation, setExploitation] = useState(false);
  const [parcours, setParcours] = useState(false);
const [suivi, setSuivi] = useState(false);

useEffect(() => {
  fetchRoleData();
}, []);

const fetchRoleData = async () => {
  try {
    const response = await axios.get('/display_components');
    const {rejeu, frequentation, exploitation, parcours, suivi } = response.data;
    setRejeu(rejeu);
    setFrequentation(frequentation);
    setExploitation(exploitation);
    setParcours(parcours);
    setSuivi(suivi);

  } catch (error) {
    console.error('Error fetching role data:', error);
  }
};
  const [darkMode, setDarkMode] = useState(false);
  const [activeIcon, setActiveIcon] = useState(null);

  const handleIconClick = (icon) => {
    setActiveIcon(icon);
    props.onIconClick(icon);
  };

 

  return (
    <div className={darkMode ? 'sidebar-container dark-mode' : 'sidebar-container'}>
      <div className={darkMode ? 'sidebar dark-mode' : 'sidebar'}>
      <div className="sidebar-icons">
        <div className="sidebar-logo ">
        <img src={Logo} alt="logo" class="logo"/>
        </div>
        <div className={`sidebar-icons ${darkMode ? 'dark-mode' : ''}`}>
          <div className={`sidebar-icon ${activeIcon === 'profil' ? 'active' : ''}`} onClick={() => handleIconClick('profil')}>
            <FaUser />
            <span>Profil</span>
          </div>

          {rejeu && (
          <div className={`sidebar-icon ${activeIcon === 'rejeu' ? 'active' : ''}`} onClick={() => handleIconClick('rejeu')}>
            <FaRedo />
            <span>Rejeu</span>
          </div> )}
          {frequentation && (
          <div className={`sidebar-icon ${activeIcon === 'frequentation' ? 'active' : ''}`} onClick={() => handleIconClick('frequentation')}>
            <FaUserCheck/>
            <span>Frequentation</span>
          </div>)}

          {exploitation && (
          <div className={`sidebar-icon ${activeIcon === 'exploitation' ? 'active' : ''}`} onClick={() => handleIconClick('exploitation')}>
            <FaHourglassStart/>
            <span>Exploitation</span>
          </div>)}
      
      {parcours && (
      <div className={`sidebar-icon ${activeIcon === 'parcours' ? 'active' : ''}`} onClick={() => handleIconClick('parcours')}>
        <FaTram/>
        <span>Parcours</span>
      </div>
    )}

    {suivi && (
      <div className={`sidebar-icon ${activeIcon === 'suivi' ? 'active' : ''}`} onClick={() => handleIconClick('suivi')}>
        <FaChartBar/>
        <span>Suivi</span>
      </div>
    )}
  </div>
          
    
  <Link to="/" className="sidebar-icon logout-icon">
  <FaSignOutAlt />
  <span>Déconnexion</span>
</Link>

          
        </div>
      </div>

      </div>

  );
};

export default SideBar;
