import React, { useState } from 'react';
import SideBar from './SideBar'
import '../Admin/DashboardAdmin.css'
import Profil from '../Admin/profil';
import Rejeu from '../ModuleRejeu/Rejeu';
import Frequentation from '../ModuleFrequentation/Frequentation'
import Exploitation from '../ModuleExploitation/Exploitation'
import Parcours from '../ModuleParcours/Parcours'
import Suivi from '../ModuleSuivi/Suivi'

function DashboardUser() {
  const [activeOption, setActiveOption] = useState(null);

  const handleIconClick = (icon) => {
    setActiveOption(icon);
  };

  const renderRightPanel = () => {
    switch (activeOption) {
      case 'profil':
        return <Profil/>;
      case 'rejeu':
        return <Rejeu/>;
      case 'frequentation':
        return <Frequentation/>;
      case 'exploitation':
        return <Exploitation/>;
      case 'parcours':
         return <Parcours/>;
      case 'suivi':
        return <Suivi/>;
      default:
        return null;
    }
  };

  return (
    <div>
      <div className="leftPanel">
      <SideBar onIconClick={handleIconClick} />
      </div>
    
      <div className="rightPanel">
        {renderRightPanel()}
      </div>
    </div>
  );
};


export default DashboardUser;
