import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Chart from "react-apexcharts";
import { makeStyles } from '@material-ui/core/styles';
import { Pagination } from '@material-ui/lab';
import { FaArrowDown,FaArrowUp} from 'react-icons/fa';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import { Search as SearchIcon } from '@material-ui/icons';

const useStyles = makeStyles({
  table: {
    minWidth: 550,
    maxWidth: '100%'
  },
  TableContainer: {
    minWidth: '55%',
    maxWidth: '100%',
    backgroundColor: '#f5f5f5', // Ajoutez cette ligne pour définir la couleur de fond en gris
  },
  icon: {
    color: '#0f2b4a',
  },
  transparent: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '1rem',
  },
  search: {
    marginBottom: '1rem',
  },
  pagination: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: '1rem',
  },
  button: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 0,
    borderRadius: '0.5rem',
    backgroundColor: '#18B1AC',
    border: 'none',
    padding: '0.5rem 1rem',
    position: 'absolute',
    right: '0',
    marginRight: '5%',
  },
});

const initialData = [
 
  // data
];

const SuiviSv = ({ date, setDate })  => {
  const classes = useStyles();
  const [users, setUsers] = useState(initialData);
  const [filterText, setFilterText] = useState('');
  const [currentPage, setCurrentPage] = useState(0);
  const ITEMS_PER_PAGE = 4;
  const [sortBy, setSortBy] = useState(null);
  const [sortOrder, setSortOrder] = useState('asc');
  const [tramData, setTramData] = useState([]);
  

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `http://localhost:5000/jourTram/${date}` 
      );
      setUsers(result.data);
      const tramResult = await axios.get(
        `http://localhost:5000/tram-data/${date}`
      );
    setTramData(tramResult.data);
    };
    fetchData();
  }, [date]);


  const handleFilterChange = (event) => {
    setFilterText(event.target.value);
    setCurrentPage(0); // reset page number to 0 when filter text changes
  };

  const handleSort = (column) => {
    if (sortBy === column) {
      // Toggle sort order if the same column is clicked again
      setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc');
    } else {
      setSortBy(column);
      setSortOrder('asc');
    }
  };

  const filteredUsers = users.filter((user) => {
    const tramNumber = Number(user.tram);
    if (isNaN(tramNumber)) {
      return false; // user.tram n'est pas un nombre, donc il ne passe pas le filtre
    }
    return tramNumber.toString().includes(filterText.toLowerCase());
  });
  
  const sortedUsers = filteredUsers.sort((a, b) => {
    if (sortBy === 'com') {
      const comA = typeof a.com === 'number' ? a.com : 0;
      const comB = typeof b.com === 'number' ? b.com : 0;
      return sortOrder === 'asc' ? comA - comB : comB - comA;
    } else if (sortBy === 'hlp') {
      const hlpA = typeof a.hlp === 'number' ? a.hlp : 0;
      const hlpB = typeof b.hlp === 'number' ? b.hlp : 0;
      return sortOrder === 'asc' ? hlpA - hlpB : hlpB - hlpA;
    }
    return 0;
  });
  
  const startIndex = currentPage * ITEMS_PER_PAGE;
  const endIndex = startIndex + ITEMS_PER_PAGE;
  const slicedUsers = sortedUsers.slice(startIndex, endIndex);
  const chartData = {
    options: {
      chart: {
        id: "DureeCourseJour",
        type: 'bar',
      },
      plotOptions: {
        bar: {
        horizontal: false,
        columnWidth: "30%",
        },
        },
      xaxis: {
        categories: tramData.map((item) => `Tram ${item.tram}`),
        title: {
          text: "Trams",
          },
      },
      dataLabels: {
        enabled:false,
      },

      yaxis: {
        title: {
          text: "Durée de course",
          },
          labels: {
            
            formatter: (value) => {
              const hours = Math.floor(value / 3600);
              return `${hours.toString().padStart(2, '0')}:00:00`;
            },
          
        },
      },
      colors: ["#18B1AC"],
      tooltip: {
        y: {
          formatter: (value) => {
            const hours = Math.floor(value / 3600);
            const minutes = Math.floor((value % 3600) / 60);
            const seconds = value % 60;
            return `${hours.toString().padStart(2, '0')}:${minutes
              .toString()
              .padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
          },
        },
      },
    },
    series: [
      {
        name: 'Durée de course',
        data: tramData.map((item) => item.duree_course),

      },
    ],
  
  };
  
  
  return (
  <>
  <div className="container-tab">
  <div className={classes.header}>
  <h3 style={{ color: '#0f2b4a' }}>Suivi de kilométrage des trams</h3>
  </div>
  <TableContainer className={classes.TableContainer} component={Paper}>
  <Table className={classes.table} aria-label="user table">
  <TableHead>
  <TableRow>
  <TableCell>
  <TextField
  className={classes.search}
  size="small"
  label="Tram"
  variant="outlined"
  onChange={handleFilterChange}
  InputProps={{
  startAdornment: (
  <InputAdornment position="start">
  <SearchIcon />
  </InputAdornment>
  ),
  }}
  />
  </TableCell>
  <TableCell
  onClick={() => handleSort('com')}
  style={{ cursor: 'pointer' }}
  >
  <b style={{ color: '#0f2b4a' }}>Commercial</b>
  {sortOrder === 'asc' && (
      <FaArrowUp style={{ marginLeft: '1px',color: '#0f2b4a'  }} />
    )}
    {sortOrder === 'desc' && (
      <FaArrowDown style={{ marginLeft: '1px',color: '#0f2b4a'  }} />
    )}
  </TableCell>
  <TableCell
  onClick={() => handleSort('hlp')}
  style={{ cursor: 'pointer' }}
  >
  <b style={{ color: '#0f2b4a' }}>Sans voyageurs</b>
  {sortOrder === 'asc' && (
      <FaArrowUp style={{ marginLeft: '1px',color: '#0f2b4a' }} />
    )}
    {sortOrder === 'desc' && (
      <FaArrowDown style={{ marginLeft: '1px' ,color: '#0f2b4a' }} />
    )}
  </TableCell>
  </TableRow>
  </TableHead>
  <TableBody>
  {slicedUsers.map((user) => (
  <TableRow key={user.id}>
  <TableCell component="th" scope="row">
  Tram {user.tram}
  </TableCell>
  <TableCell>{user.com? user.com: 0.00}</TableCell>
  <TableCell>{user.hlp? user.hlp: 0.00}</TableCell>
  </TableRow>
  ))}
  </TableBody>
  </Table>
  <div className={classes.pagination}>
  <Pagination
  count={Math.ceil(filteredUsers.length / ITEMS_PER_PAGE)}
  onChange={(event, value) => setCurrentPage(value - 1)}
  />
  </div>
  </TableContainer>
  
  </div>
  <div className="container-tab">
  
    <h3  style={{color: '#0f2b4a'}}>Durée de courses par tram</h3>
    <br/><br/>
     <Chart
        options={chartData.options}
        series={chartData.series}
        type="bar"
        height={450}
        width={700}
      />
    </div>
  
  </>
  
  );
  };
  
  export default SuiviSv;