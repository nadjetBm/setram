import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { Pagination } from '@material-ui/lab';
import { FaArrowDown,FaArrowUp} from 'react-icons/fa';

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import { Search as SearchIcon } from '@material-ui/icons';

const useStyles = makeStyles({
  table: {
    minWidth: 550,
    maxWidth: '100%'
  },
  TableContainer: {
    minWidth: '55%',
    maxWidth: '100%',
    backgroundColor: '#f5f5f5', // Ajoutez cette ligne pour définir la couleur de fond en gris
  },
  icon: {
    color: '#0f2b4a',
  },
  transparent: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '1rem',
  },
  search: {
    marginBottom: '1rem',
  },
  pagination: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: '1rem',
  },
  button: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 0,
    borderRadius: '0.5rem',
    backgroundColor: '#18B1AC',
    border: 'none',
    padding: '0.5rem 1rem',
    position: 'absolute',
    right: '0',
    marginRight: '5%',
  },
});

const initialData = [
 
  // data
];

const SuiviConducteur = ({ date, setDate })  => {
  const classes = useStyles();
  const [users, setUsers] = useState(initialData);
  const [filterText, setFilterText] = useState('');
  const [currentPage, setCurrentPage] = useState(0);
  const ITEMS_PER_PAGE = 9;
  const [sortBy, setSortBy] = useState(null);
  const [sortOrder, setSortOrder] = useState('asc');

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `http://localhost:5000/jourConducteur/${date}` 
      );
      setUsers(result.data);
    };
    fetchData();
  }, [date]);


  const handleFilterChange = (event) => {
    setFilterText(event.target.value);
    setCurrentPage(0); // reset page number to 0 when filter text changes
  };

  const handleSort = (column) => {
    if (sortBy === column) {
      // Toggle sort order if the same column is clicked again
      setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc');
    } else {
      setSortBy(column);
      setSortOrder('asc');
    }
  };

const filteredUsers = users.filter((user) =>
   
user.nom_prenom.toLowerCase().includes(filterText.toLowerCase())
);

const sortedUsers = filteredUsers.sort((a, b) => {
  if (sortBy === 'duree_conduite') {
  return sortOrder === 'asc'
  ? a.duree_conduite.localeCompare(b.duree_conduite)
  : b.duree_conduite.localeCompare(a.duree_conduite);
  }  else if (sortBy === 'habilitation') {
    return sortOrder === 'asc'
      ? a.habilitation.localeCompare(b.habilitation)
      : b.habilitation.localeCompare(a.habilitation);
  }
  return 0;
  });
  
  const startIndex = currentPage * ITEMS_PER_PAGE;
  const endIndex = startIndex + ITEMS_PER_PAGE;
  const slicedUsers = sortedUsers.slice(startIndex, endIndex);
  
  
  return (
  <>
  <div className="container-tab">
  <div className={classes.header}>
  <h3 style={{ color: '#0f2b4a' }}>Liste des conducteurs</h3>
  </div>
  <TableContainer className={classes.TableContainer} component={Paper}>
  <Table className={classes.table} aria-label="user table">
  <TableHead>
  <TableRow>
  <TableCell>
  <TextField
  className={classes.search}
  size="small"
  label="Nom prénom"
  variant="outlined"
  onChange={handleFilterChange}
  InputProps={{
  startAdornment: (
  <InputAdornment position="start">
  <SearchIcon />
  </InputAdornment>
  ),
  }}
  />
  </TableCell>
  <TableCell
  onClick={() => handleSort('duree_conduite')}
  style={{ cursor: 'pointer' }}
  >
  <b style={{ color: '#0f2b4a' }}>Temps de conduite</b>
  {sortOrder === 'asc' && (
      <FaArrowUp style={{ marginLeft: '1px',color: '#0f2b4a'  }} />
    )}
    {sortOrder === 'desc' && (
      <FaArrowDown style={{ marginLeft: '1px',color: '#0f2b4a'  }} />
    )}
  </TableCell>
  <TableCell
 
  style={{ cursor: 'pointer' }}
  >
  <b style={{ color: '#0f2b4a' }}>Maintien d'habilitation</b>
  
  </TableCell>
  </TableRow>
  </TableHead>
  <TableBody>
  {slicedUsers.map((user) => (
  <TableRow key={user.id}>
  <TableCell component="th" scope="row">
  {user.nom_prenom}
  </TableCell>
  <TableCell>{user.duree_conduite}</TableCell>
  <TableCell>{user.habilitation}</TableCell>
  </TableRow>
  ))}
  </TableBody>
  </Table>
  <div className={classes.pagination}>
  <Pagination
  count={Math.ceil(filteredUsers.length / ITEMS_PER_PAGE)}
  onChange={(event, value) => setCurrentPage(value - 1)}
  />
  </div>
  </TableContainer>
  </div>
  </>
  );
  };
  
  export default SuiviConducteur;