import React, { useState, useEffect } from "react";
import axios from "axios";
import Chart from "react-apexcharts";

const SuiviAlarme = ({ date, setDate }) => {
const [data, setData] = useState({});
const [alarmeHoraireData, setAlarmeHoraireData] = useState([]);
const [alarmeTramData, setAlarmeTramData] = useState([]);


useEffect(() => {
const fetchData = async () => {

  const alarmeHoraireResult = await axios.get(
    `http://localhost:5000/jourHoraireAlarme/${date}`
  );
setAlarmeHoraireData(alarmeHoraireResult.data);

const alarmeTramResult = await axios.get(
  `http://localhost:5000/jourtramAlarme/${date}`
);
setAlarmeTramData(alarmeTramResult.data);

};

fetchData();
}, [date]);







  const AlarmeHoraireChart = {
    chart: {
      id: "AlarmeHoraireChart",
      type: "bar",
      height: 350,
    },
    plotOptions: {
      bar: {
      horizontal: false,
      columnWidth: "50%",
      },
      },
    xaxis: {
      title: {
        text: "Tranche horaire",
        },
      categories: alarmeHoraireData.map((item) => item.tranche_horaire),
      
    },
    dataLabels: {
      enabled:false,
    },
    yaxis: {
      title: {
      text: "Nombre des alarmes",
      },
      },
      colors:["#18B1AC","#0f2b4a","#FFA500"],
     
   
      
    series: [
      {
        name: "Activation poignées de déverrouillage d'urgence",
        data: alarmeHoraireData.map((item) => item["Activation poignées de déverrouillage d'urgence"]),
      },
      {
        name: "Freinage de sécurité",
        data: alarmeHoraireData.map((item) => item["Freinage de sécurité"]),
      },
      {
        name: "Freinage d'urgence",
        data: alarmeHoraireData.map((item) => item["Freinage d'urgence"]),
      },
    ],
  };
  
 
  const AlarmeTramChart = {
    chart: {
      id: "AlarmeTramChart",
      type: "bar",
      height: 350,
     
    },
    xaxis: {
      title: {
        text: "Tram",
        },
      categories: alarmeTramData.map((item) =>`Tram ${ item.tram_alarme}`),
      
    },
    yaxis: {
      title: {
      text: "Nombre des alarmes",
      },
      },
      dataLabels: {
        enabled:false,
      },
      
      colors:["#18B1AC","#0f2b4a","#FFA500"],
     
    series: [
      {
        name: "Activation poignées de déverrouillage d'urgence",
        data: alarmeTramData.map((item) => item["Activation poignées de déverrouillage d'urgence"]),
      
      },
      {
        name: "Freinage de sécurité",
        data: alarmeTramData.map((item) => item["Freinage de sécurité"]),
      
      },
      {
        name: "Freinage d'urgence",
        data: alarmeTramData.map((item) => item["Freinage d'urgence"]),
        
      },
    ],
  };


  
return (
<div>

<div className="container-bar">
  <h3 style={{color: '#0f2b4a'}}>Nombre des alarmes par tram</h3>
  <Chart
     options={AlarmeTramChart} series={AlarmeTramChart.series} type="bar" height={450}  width={750}
    />
  </div>
  <div className="container-bar">
  <h3 style={{color: '#0f2b4a'}}>Nombre des alarmes par tranche horaire</h3>
  <Chart
     options={AlarmeHoraireChart} series={AlarmeHoraireChart.series} type="line" height={450}  width={750}
    />
  </div>

</div>
);
};

export default SuiviAlarme;