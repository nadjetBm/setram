
import SuiviAlarme from "./SuiviAlarme"
import SuiviAlarmePeriode from "./SuiviAlarmePeriode";
import SuiviSvPeriode from "./SuiviSvPeriode";
import SuiviSv from "./SuiviSv";
import SuiviConducteur from "./SuiviConducteur"
import SuiviConducteurPeriode from "./SuiviConducteurPeriode"
import SansIndicateur from "../Common/SansIndicateurs"
import './SideBarSuivi.css'
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import moment from 'moment';
import React, { useState, useRef } from "react";
import html2canvas from 'html2canvas'
import {FaImage} from 'react-icons/fa';


const SidBarSuivi= () => {

  const [date, setDate] = useState('');
  const [showProgressBar, setShowProgressBar] = useState(false);
  const [showMoyalibComponent, setShowMoyalibComponent] = useState(false);
  const [showAgent, setShowAgent] = useState(false);
  const [showSansIndicateur, setShowSansIndicateur] = useState(true);

  const containerRef = useRef(null);
  const saveContainerAsImage = () => {
    html2canvas(containerRef.current).then((canvas) => {
      const image = canvas.toDataURL('image/png'); // Convert canvas to base64 image URL
      const link = document.createElement('a');
      link.href = image;
      link.download =`${date}||${startDate}-${endDate}.png`; // Set the downloaded file name
      link.click();
    });
  };
  
  
  const handleChange = (event) => {
    setDate(event.target.value);
    setStartDate("");
    setEndDate("");
  };

  const handleDatesChange = ({ startDate, endDate }) => {
    setStartDate(startDate);
    setEndDate(endDate);
    setDate("");
  };

  const handleProgressBarChange = (event) => {
    setShowProgressBar(event.target.checked);
    setShowMoyalibComponent(false); 
    setShowAgent(false);
    setShowSansIndicateur(!(event.target.checked || showMoyalibComponent || showAgent));
  };
  
  const handleAgentChange = (event) => {
    setShowAgent(event.target.checked);
    setShowMoyalibComponent(false); 
    setShowProgressBar(false);
    setShowSansIndicateur(!(event.target.checked || showMoyalibComponent || showProgressBar));
  };
  
  const handleMoyalibComponentChange = (event) => {
    setShowMoyalibComponent(event.target.checked);
    setShowProgressBar(false);
    setShowAgent(false);
    setShowSansIndicateur(!(event.target.checked || showProgressBar || showAgent));
  };

  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [focusedInput, setFocusedInput] = useState('');

  return (
    <div className="parent-container">
      <div className="left-container">
        <h3 style={{color: '#0f2b4a'}}>Module Suivi</h3><br/>
        <p style={{color: '#0f2b4a'}}><b>Paramètres temporels</b></p>
        <p style={{color: '#0f2b4a'}}>Date</p>
        <input type="date" value={date} onChange={handleChange} />
        <br/>
        <p style={{color: '#0f2b4a'}}>Période</p>
      <div className="datePicker"> 
      <DateRangePicker
  startDate={startDate}
  endDate={endDate}
  onDatesChange={handleDatesChange}
  focusedInput={focusedInput}
  onFocusChange={focusedInput => setFocusedInput(focusedInput)}
  startDatePlaceholderText="Date début -"
  endDatePlaceholderText="Date fin"
  minimumNights={0}
  small={true}
  isOutsideRange={(day) => day.isAfter(moment(), 'day')}
/>
      </div>
        <br/><br/><br/><br/>
        <p style={{color: '#0f2b4a'}}><b>Agrégation</b></p>
        <input id="myCheckbox" type="checkbox" checked={showAgent} onChange={handleAgentChange} />
          <label for="myCheckbox">
            Conducteurs
        </label>
        <br/>
          <input id="myCheckbox" type="checkbox" checked={showProgressBar} onChange={handleProgressBarChange} />
          <label for="myCheckbox">
          Alarmes
          </label>
       
          <br/>
       
          <input id="myCheckbox" type="checkbox" checked={showMoyalibComponent} onChange={handleMoyalibComponentChange} />
          <label for="myCheckbox">
          Trams
        </label>
        <button className="export-button" onClick={saveContainerAsImage}><i className="fas fa-file-export"></i>Capture &nbsp;<FaImage/></button>  </div>
      <div className="right-container" ref={containerRef}>
        {!showProgressBar && !showMoyalibComponent && !showAgent && showSansIndicateur && <SansIndicateur />}
        {date && showProgressBar && <SuiviAlarme date={date} setDate={setDate} />  }
        {date && showMoyalibComponent && <SuiviSv date={date} setDate={setDate} />}
        {startDate && showMoyalibComponent && <SuiviSvPeriode startDate={startDate} endDate={endDate}/>}
        {startDate && showProgressBar && <SuiviAlarmePeriode startDate={startDate} endDate={endDate} />}
        {date && showAgent && <SuiviConducteur date={date} setDate={setDate} />  }
        {startDate && showAgent && <SuiviConducteurPeriode startDate={startDate} endDate={endDate} />}

        
      </div>
    </div>
  );
};


export default SidBarSuivi;

