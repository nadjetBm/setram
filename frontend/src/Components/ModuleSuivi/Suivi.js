
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import{Routes,Route} from 'react-router-dom' 
import PasAcces from '../Common/PasAcces'
import SideBarSuivi from './SideBarSuivi'
import SuiviConducteur from './SuiviConducteur'
import SuiviSv from './SuiviSv'
import SuiviAlarme from './SuiviAlarme'
function Suivi()
{
    const [suivi, setSuivi] = useState(false);
    
    useEffect(() => {
      fetchRoleData();
    }, []);
    
    const fetchRoleData = async () => {
      try {
        const response = await axios.get('/display_components');
        const { suivi } = response.data;
        setSuivi(suivi);
       
      } catch (error) {
        console.error('Error fetching role data:', error);
      }
    };
    return(
        <React.Fragment>

    <React.Fragment>
      <SideBarSuivi />
    </React.Fragment>

        </React.Fragment>
    )
}
export default Suivi;