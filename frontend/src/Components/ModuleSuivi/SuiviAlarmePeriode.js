import React, { useState, useEffect } from "react";
import axios from "axios";
import Chart from "react-apexcharts";

const SuiviAlarmePeriode = ({ startDate, setStartDate,endDate, setEndDate }) => {
const [data, setData] = useState({});
const [alarmeHorairePeriodeData, setAlarmeHorairePeriodeData] = useState([]);
const [alarmeTramPeriodeData, setAlarmeTramPeriodeData] = useState([]);


useEffect(() => {
const fetchData = async () => {

  const alarmeHorairePeriodeResult = await axios.get(
    `http://localhost:5000/periodeHoraireAlarme/${startDate}/${endDate}}`
  );
setAlarmeHorairePeriodeData(alarmeHorairePeriodeResult.data);

const alarmeTramPeriodeResult = await axios.get(
  `http://localhost:5000/periodeTramAlarme/${startDate}/${endDate}`
);
setAlarmeTramPeriodeData(alarmeTramPeriodeResult.data);

};

fetchData();
}, [startDate,endDate]);







  const AlarmeHorairePeriodeChart = {
    chart: {
      id: "AlarmeHorairePeriodeChart",
      type: "bar",
      height: 350,
    },
    plotOptions: {
      bar: {
      horizontal: false,
      columnWidth: "50%",
      },
      },
      dataLabels: {
        enabled:false,
      },
    
    xaxis: {
      title: {
        text: "Tranche horaire",
        },
      categories: alarmeHorairePeriodeData.map((item) => item.tranche_horaire),
      
    },
    yaxis: {
      title: {
      text: "Nombre des alarmes",
      },
      },
      colors:["#18B1AC","#0f2b4a","#FFA500"],
     
      
    series: [
      {
        name: "Activation poignées de déverrouillage d'urgence",
        data: alarmeHorairePeriodeData.map((item) => item["Activation poignées de déverrouillage d'urgence"]),
      },
      {
        name: "Freinage de sécurité",
        data: alarmeHorairePeriodeData.map((item) => item["Freinage de sécurité"]),
      },
      {
        name: "Freinage d'urgence",
        data: alarmeHorairePeriodeData.map((item) => item["Freinage d'urgence"]),
      },
    ],
  };
  
 
  const AlarmeTramPeriodeChart = {
    chart: {
      id: "AlarmeTramPeiodeChart",
      type: "bar",
      height: 350,
    },
    plotOptions: {
      bar: {
      horizontal: false,
      columnWidth: "50%",
      },
      },
    xaxis: {
      title: {
        text: "Tram",
        },
      categories: alarmeTramPeriodeData.map((item) => `Tram ${ item.tram_alarme}`),

      
    },
    dataLabels: {
      enabled:false,
    },
    yaxis: {
      title: {
      text: "Nombre des alarmes",
      },
      },
      colors:["#18B1AC","#0f2b4a","#FFA500"],
      
    series: [
      {
        name: "Activation poignées de déverrouillage d'urgence",
        data: alarmeTramPeriodeData.map((item) => item["Activation poignées de déverrouillage d'urgence"]),
      },
      {
        name: "Freinage de sécurité",
        data: alarmeTramPeriodeData.map((item) => item["Freinage de sécurité"]),
      },
      {
        name: "Freinage d'urgence",
        data: alarmeTramPeriodeData.map((item) => item["Freinage d'urgence"]),
      },
    ],
  };


  
return (
<div>

<div className="container-bar">
  <h3 style={{color: '#0f2b4a'}}>Nombre des alarmes par tram</h3>
  <Chart
     options={AlarmeTramPeriodeChart} series={AlarmeTramPeriodeChart.series} type="bar" height={450}  width={750}
    />
  </div>
  <div className="container-bar">
  <h3 style={{color: '#0f2b4a'}}>Nombre des alarmes par tranche horaire</h3>
  <Chart
     options={AlarmeHorairePeriodeChart} series={AlarmeHorairePeriodeChart.series} type="line" height={450}  width={750}
    />
  </div>

</div>
);
};

export default SuiviAlarmePeriode;