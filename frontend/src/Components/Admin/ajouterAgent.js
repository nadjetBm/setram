import React, {  useState ,useEffect} from 'react';
import axios from 'axios';
import { FaUser, FaEnvelope, FaUsers, FaIdCard, FaLock } from 'react-icons/fa';
import './ajouter.css';


const Ajouter = () => {
  const [nom, setNom] = useState('');
  const [role, setRole] = useState('');
  const [email, setEmail] = useState('');
  const [matricule, setMatricule] = useState('');
  const [message, setMessage] = useState('');
  const [addMessage, setAddMessage] = useState('');
  const [options, setOptions] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('http://localhost:5000/afficherRole', {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      });
      setOptions(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  
  const handleSubmit = async (e) => {
    
    e.preventDefault();
  
    
    // Send the data to the backend to register the user
    try {
      const response = await fetch('/AjouterAgent', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          nom_prenom: nom,
          email: email,
          matricule: matricule,
          role_compte: role,
        }),
      });
  
      const data = await response.json();
      console.log('Response:', data);
      // Handle the response from the backend
      if (data.success) {
        setAddMessage('Le compte a été ajouté avec succès!');
        
      } else {
        setMessage(data.message);
   
      }
    } catch (error) {
      console.error('Error:', error);
      // Handle any network or server errors
    }
    
  };

  
  



  return (
    <div>
    <div className="container">
      <h2>Créer un compte</h2>
      <br></br>
      <form onSubmit={handleSubmit} className="form-container">
     
       
      <label htmlFor="matricule">
          Matricule
          <div class="input-container">
          <FaIdCard className='icon'/>
          <input
            id="matricule"
            type="text"
            value={matricule}
            onChange={(e) => setMatricule(e.target.value)}
            className="input-field"
            required
          />
          </div>
         
        </label>
        {((matricule.length === 5 && /^\d+$/.test(matricule))) || !(matricule.length === 0) &&  (
        <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
          <p  style={{color: 'red',marginTop: '-10px'}}>Matricule invalide</p>
        </div>
      )}
        <label htmlFor="nom">
          Nom prénom
          <div class="input-container">
          <FaUser className='icon'/>
          <input
            id="nom"
            type="text"
            value={nom}
            onChange={(e) => setNom(e.target.value)}
            className="input-field"
            required
          />
          </div>
          
        </label>
        <label htmlFor="email">
          Email
          <div class="input-container">
          <FaEnvelope className='icon'/>
          <input
            id="email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="input-field"
            required
          />
           
          </div>
          
        </label>
        {!(email === '') && !email.endsWith('@setram-sba.dz') &&(
           
      <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
        <p  style={{color: 'red',marginTop: '-10px'}}>L'adresse e-mail doit se terminer par '@setram-sba.dz'.
        </p> </div>)}
        
        <label htmlFor="role">
  Rôle
  <div class="input-container">
    <FaUsers className='icon'/>
    <select className="input-field" onChange={(e) => setRole(e.target.value)}  required>
    <option value="" selected disabled>Sélectionnez un rôle</option>
      {options.map(option => (
        <option key={option.id} value={option.id}>
          {option.role_name}
        </option>
      ))}
    </select>
  </div>
</label>
<br/>
       
        <button type="submit" className="submit-btn">
          Créer un compte
        </button>
        {addMessage && <b  style={{color: '#18B1AC'}}>{addMessage}</b>}
        {message && <p  style={{color: 'red'}}>{message}</p>}
        
      </form>
    </div>
    </div>
  );
};

export default Ajouter;
