import React, { useState } from 'react';
import SideBarAdmin from './SideBarAdmin';
import Conducteurs from './conducteurs';
import Profil from './profil';
import Utilisateurs from './utilisateurs';
import Role from './roles';
import Ajouter from './ajouterAgent';
import './DashboardAdmin.css'


function DashboardAdmin() {
  const [activeOption, setActiveOption] = useState(null);

  const handleIconClick = (icon) => {
    setActiveOption(icon);
  };

  const renderRightPanel = () => {
    switch (activeOption) {
      case 'profil':
        return <Profil />;
      case 'utilisateurs':
        return <Utilisateurs/>;
      case 'conducteurs':
        return <Conducteurs />;
      case 'roles':
        return <Role/>;
      case 'ajouter':
        return <Ajouter/>;
      default:
        return null;
    }
  };

  return (
    <div>
      <div className="leftPanel">
      <SideBarAdmin onIconClick={handleIconClick} />
      </div>
    
      <div className="rightPanel">
        {renderRightPanel()}
      </div>
    </div>
  );
};

export default DashboardAdmin;