import React, {  useState ,useEffect} from 'react';
import axios from 'axios';
import './ajouter.css';
import { FaUser, FaEnvelope, FaUsers, FaIdCard, FaLock } from 'react-icons/fa';
const RoleForm = ({ user, handleClose }) => {
    const [email, setEmail] = useState(user.email);
    const [matricule, setMatricule] = useState(user.matricule);
    const [role, setRole] = useState(user.role_compte);
    const [compte, setCompte] = useState(user.compte);
    const [nomPrenom, setNomPrenom] = useState(user.nom_prenom);
    const [successMessage, setSuccessMessage] = useState('');
    const [options, setOptions] = useState([]);
  
    const handleSubmit = (e) => {
        e.preventDefault();
        fetch(`/modifier_compte/${nomPrenom}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            matricule: matricule,
            role: role,
            compte: compte,
            email: email
          }),
        })
          .then(response => {
            // Traitez la réponse du serveur
            if (response.ok) {
              setSuccessMessage('Le compte a été modifié avec succès!');
            } else {
              setSuccessMessage('Une erreur s\'est produite lors de la modification du compte.');
            }
          })
          .catch(error => {
            // Gérez les erreurs
            setSuccessMessage("Une erreur s'est produite lors de la modification du compte.");
          });
      };
      
    useEffect(() => {
        fetchData();
      }, []);
    
    const fetchData = async () => {
        try {
          const response = await axios.get('http://localhost:5000/afficherRole', {
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          setOptions(response.data);
        } catch (error) {
          console.error(error);
        }
      };
  return (
    <div className="container">
      <h2>Modifier un compte</h2>
      <br />
    <form  className="form-container-modifier" onSubmit={handleSubmit} >
    <label>
        Matricule
          <div class="input-container">
          <FaIdCard className='icon'/>
          <input type="text"  className="input-field" name="matricule" value={matricule} onChange={e => setMatricule(e.target.value)}/>
           </div>
      </label>
       {((/^\d+$/.test(matricule))) || !(matricule.length === 0) &&  (
        <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
          <p  style={{color: 'red',marginTop: '-10px'}}>Matricule invalide</p>
        </div>
      )}
      <br />
      <label>
        Nom prénom
          <div class="input-container">
          <FaUser className='icon'/>
       <input type="text"  className="input-field" name="nom_prenom" value={nomPrenom} onChange={e => setNomPrenom(e.target.value)} />
       </div>
       </label>
      <br />
      <label>
      Email
          <div class="input-container">
          <FaEnvelope className='icon'/>
        <input type="text"  className="input-field" name="email" value={email} onChange={e => setEmail(e.target.value)} />
      </div>
      </label>
      {!(email === '') && !email.endsWith('@setram-sba.dz') &&(
           
           <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
             <p  style={{color: 'red',marginTop: '-10px'}}>L'adresse e-mail doit se terminer par '@setram-sba.dz'.
             </p> </div>)}
             
      <br />
     
      <label>
      Rôle
  <div class="input-container">
    <FaUsers className='icon'/>
   
    <select className="input-field" onChange={(e) => setRole(e.target.value)}  required>
    <option value={role} selected disabled>{role}</option>
      {options.map(option => (
        <option key={option.id} value={option.id}>
          {option.role_name}
        </option>
      ))}
    </select>
  </div>
 
    
      </label>
      <br />
      <div className="checkbox-role">
      <input type="checkbox" name="compte" checked={compte} onChange={e => setCompte(e.target.checked)} />
      <label style={{ fontSize :'18px', marginLeft : '10px' }}>
      {compte ? "Compte activé" : "Compte désactivé"}
        </label>
      </div>
      <br /><br />

      <button type="submit" className="submit-btn">Modifier un rôle</button>

      {successMessage && <p>{successMessage}</p>}
    </form>
    </div>
  );
};

export default RoleForm;