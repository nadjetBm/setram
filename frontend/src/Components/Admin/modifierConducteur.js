import React, {  useState ,useEffect} from 'react';
import axios from 'axios';
import './ajouter.css';
import { FaUser, FaEnvelope, FaUsers, FaIdCard, FaLock } from 'react-icons/fa';
const RoleForm = ({ user, handleClose }) => {
    const [matricule_agent, setMatricule] = useState(user.matricule_agent);
    const [nomPrenom, setNomPrenom] = useState(user.nom_prenom);
    const [idAgent, setIdAgent] = useState(user.id_tab_agent_habilite);
    const [successMessage, setSuccessMessage] = useState('');
    const [options, setOptions] = useState([]);
  
    const handleSubmit = (e) => {
        e.preventDefault();
        fetch(`/modifier_conducteur/${idAgent}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            matricule_agent: matricule_agent,
            nom_prenom:nomPrenom ,
          }),
        })
          .then(response => {
            // Traitez la réponse du serveur
            if (response.ok) {
              setSuccessMessage('Les informations du conducteurs a été modifié avec succès!');
            } else {
              setSuccessMessage('Une erreur s\'est produite lors de la modification des informations du conducteurs.');
            }
          })
          .catch(error => {
            // Gérez les erreurs
            setSuccessMessage("Une erreur s'est produite lors de la modification des informations du conducteurs.");
          });
      };
      
    useEffect(() => {
        fetchData();
      }, []);
    
    const fetchData = async () => {
        try {
          const response = await axios.get('http://localhost:5000/afficherRole', {
            headers: {
              'Access-Control-Allow-Origin': '*',
            },
          });
          setOptions(response.data);
        } catch (error) {
          console.error(error);
        }
      };
  return (
    <div className="container">
      <h2 style={{ fontSize: '18px'}}>Modifier les informations du conducteurs</h2>
      <br />
    <form  className="form-container-modifier" onSubmit={handleSubmit} >
    <label>
        Matricule
          <div class="input-container">
          <FaIdCard className='icon'/>
          <input type="text"  className="input-field" name="matricule_agent" value={matricule_agent} onChange={e => setMatricule(e.target.value)}/>
           </div>
      </label>
       {((/^\d+$/.test(matricule_agent))) || !(matricule_agent.length === 0) &&  (
        <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
          <p  style={{color: 'red',marginTop: '-10px'}}>Matricule invalide</p>
        </div>
      )}
      <br />
      <label>
        Nom prénom
          <div class="input-container">
          <FaUser className='icon'/>
       <input type="text"  className="input-field" name="nom_prenom" value={nomPrenom} onChange={e => setNomPrenom(e.target.value)} />
       </div>
       </label>
      <br />
   
      <br />

      <button type="submit" className="submit-btn">Modifier les informations </button>

      {successMessage && <p>{successMessage}</p>}
    </form>
    </div>
  );
};

export default RoleForm;