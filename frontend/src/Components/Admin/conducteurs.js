import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { Pagination } from '@material-ui/lab';
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import { MdClose } from "react-icons/md";
import './ajouter.css';
import ModifierCompte from './modifierConducteur'
import { FaUserPlus } from 'react-icons/fa';
import AjouterAgent from './ajouterConducteur'


import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import {
  Delete as DeleteIcon,
  Edit as EditIcon,
  Search as SearchIcon,
} from '@material-ui/icons';

const useStyles = makeStyles({
  table: {
    minWidth: 550,
  },
  TableContainer: {
    minWidth:'55%',
    maxWidth:'95%',
  },
  icon: {
    color: '#0f2b4a',
  },
 
  transparent: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
  },
  header: {
    display: 'flex',

    alignItems: 'center',
    marginBottom: '1rem',
  },
  search: {
    marginBottom: '1rem',
  },
  pagination: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: '1rem',
  },
  button: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 0,
    borderRadius: '0.5rem',
    backgroundColor: '#18B1AC',
    border: 'none',
    padding: '0.5rem 1rem',
    position: 'absolute',
    right: '0',
    marginRight: '5%',
  }
  
  
  
});

const initialData = [
 
];

const Conducteurs = () => {
  const classes = useStyles();
  const [users, setUsers] = useState(initialData);
  const [filterText, setFilterText] = useState('');
  const [currentPage, setCurrentPage] = useState(0);
  const ITEMS_PER_PAGE = 5;

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `http://localhost:5000/conducteur` 
      );
      setUsers(result.data);
    };
    fetchData();
  },);

  
  const handleFilterChange = (event) => {
    setFilterText(event.target.value);
    setCurrentPage(0); // reset page number to 0 when filter text changes
  };

  const filteredUsers = users.filter((user) =>
    user.nom_prenom.toLowerCase().includes(filterText.toLowerCase())
  );


  const startIndex = currentPage * ITEMS_PER_PAGE;
  const endIndex = startIndex + ITEMS_PER_PAGE;
  const slicedUsers = filteredUsers.slice(startIndex, endIndex);
  const [openDialogRole, setOpenDialogRole] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  
  const [openDialog, setOpenDialog] = useState(false);

  const handleOpenDialog = () => {
    setOpenDialog(true);
    setOpenDialogRole(false);
  };

  const handleOpenDialogRole = () => {
 
    setOpenDialogRole(true);
    
  };
  const handleEdit = (user) => {
    setOpenDialogRole(true);
    setSelectedUser(user); 
  };
  
  
  

 
  
  return (
    <>
    <Dialog open={openDialog} onClose={() => setOpenDialog(false)} aria-labelledby="form-dialog-title" >
  <DialogContent>
    <AjouterAgent/>
  </DialogContent>
  <DialogActions>
    <button onClick={() => setOpenDialog(false)} style={{position: 'absolute', top: 0, right: 0, border: 'none', background: 'transparent', cursor: 'pointer'}}>
      <MdClose style={{fontSize: '24px', fontWeight: 'bold'}} />
    </button>
  </DialogActions>
</Dialog>
<Dialog open={openDialogRole} onClose={() => setOpenDialogRole(false)} aria-labelledby="form-dialog-title" >
  <DialogContent>
  <ModifierCompte user={selectedUser} handleClose={() => setOpenDialogRole(false)} /> {/* Passez l'utilisateur sélectionné en tant que prop */}
  </DialogContent>
  <DialogActions>
  <button onClick={() => {
    setOpenDialogRole(false);
  }} style={{ position: 'absolute', top: 0, right: 0, border: 'none', background: 'transparent', cursor: 'pointer' }}>
    <MdClose style={{ fontSize: '24px', fontWeight: 'bold' }} />
  </button>
</DialogActions>
</Dialog>


      <div className={classes.header}>
      <h2 style={{ color: '#0f2b4a' }}>Gestion  des conducteurs</h2>
      <button className={classes.button} onClick={handleOpenDialog}>
  <FaUserPlus style={{ color: 'white', marginRight: '0.5rem' }} />
  <b style={{ color: 'white' }}>Ajouter un conducteur </b>
</button>
     
</div>
      <TableContainer className={classes.TableContainer} component={Paper}>
        <Table className={classes.table} aria-label="user table">
          <TableHead>
            <TableRow>
              <TableCell style={{ color: '#0f2b4a'  }}>
              <TextField
                className={classes.search}
                size="small"
                label="Nom Prénom"
                variant="outlined"
                onChange={handleFilterChange}
                InputProps={{
                startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
                ),
              }}
            />
              </TableCell>
              <TableCell style={{textAlign:'center' }}><b style={{ color: '#0f2b4a' }}>Matricule du conducteur</b></TableCell>
              
              <TableCell> <b style={{ color: '#fff' }}>Id</b></TableCell>
              <TableCell> <b style={{ color: '#fff' }}>Id</b></TableCell>
              <TableCell> <b style={{ color: '#fff' }}>Id</b></TableCell>
              <TableCell> <b style={{ color: '#fff' }}>Id</b></TableCell>
              <TableCell> <b style={{ color: '#fff' }}>Id</b></TableCell>
              <TableCell><b style={{ color: '#0f2b4a' }}>Action</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {slicedUsers.map((user) => (
              <TableRow key={user.id}>
                
                <TableCell component="th" scope="row">
                  
                  {user.nom_prenom}
                </TableCell>
                
                <TableCell style={{textAlign:'center' }}>{user.matricule_agent}</TableCell>
                <TableCell style={{textAlign:'center' }}><b style={{ color: '#fff' }}>{user.id_tab_agent_habilite}</b></TableCell>
                <TableCell style={{textAlign:'center' }}><b style={{ color: '#fff' }}>{user.id_tab_agent_habilite}</b></TableCell>
                <TableCell style={{textAlign:'center' }}><b style={{ color: '#fff' }}>{user.id_tab_agent_habilite}</b></TableCell>
                <TableCell style={{textAlign:'center' }}><b style={{ color: '#fff' }}>{user.id_tab_agent_habilite}</b></TableCell>
                <TableCell style={{textAlign:'center' }}><b style={{ color: '#fff' }}>{user.id_tab_agent_habilite}</b></TableCell>
            
                <TableCell>
                  <div className="icons" style={{ display: 'flex' }}>
                    <p
                      className={classes.transparent}
                      aria-label="edit"
                      onClick={() => handleEdit(user)}
                    >
                      <EditIcon className={classes.icon} onClick={handleOpenDialogRole}/>
                    </p>
                  </div>
                </TableCell>
                </TableRow>
            ))}
          </TableBody>
        </Table>
        <div className={classes.pagination}>
          <Pagination
            count={Math.ceil(filteredUsers.length / ITEMS_PER_PAGE)}
            
            onChange={(event, value) => setCurrentPage(value - 1)}
          />
 
        </div>
      </TableContainer>
    </>
  );
}
export default Conducteurs;