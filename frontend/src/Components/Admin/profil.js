import './profil.css';
import { FaUser } from 'react-icons/fa';
import React, { useState, useEffect } from 'react';

const Profil = () => {
  
  const [role, setRole] = useState('');
  const [password, setPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [profileData, setProfileData] = useState(null);
  const [successMessage, setSuccessMessage] = useState('');
  
  const handleSubmit = (e) => {
  e.preventDefault();
  fetch(`/modifierMotDePasse/${password}/${newPassword}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      password: password,
      newPassword: newPassword
    }),
  })
    .then(response => {
      if (response.ok) {
        return response.json(); 
      } else {
        throw new Error('Une erreur s\'est produite lors de la modification du mot de passe.');
      }
    })
    .then(data => {
      setSuccessMessage(data.message);
    })
    .catch(error => {
      setSuccessMessage("Une erreur s'est produite lors de la modification du mot de passe.");
    });
};

  

  useEffect(() => {
    fetch('/profile')
      .then(response => response.json())
      .then(data => setProfileData(data))
      .catch(error => console.log(error));
  }, []);



  if (!profileData) {
    return <div>Chargement...</div>;
  }

  if ('message' in profileData) {
    return <div>{profileData.message}</div>;
  }
 

  return (
    <div className="profile-container">
      <div className="icon-container">
        <div className="user-icon">
          <FaUser />
        </div>
      </div>
      <div className="name-container">
        <div style={{textAlign:'center'}}
        ><h2 style={{ color: '#0f2b4a'}}>{profileData.nom_prenom}</h2>
        <p style={{ color: '#0f2b4a', marginTop: -20 }}>{profileData.role_compte}</p>

        </div>
       
       
      </div>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <div
            className="form-row"
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}
          >
            
          </div>
          <div
            className="form-row"
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}
          >
          
          <div style={{ flexGrow: 1, marginRight: '10px' }}>
            <label htmlFor="password-input">Mot de passe actuel</label><br/>
            <input
              type="password"
              id="password-input"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
            {password != profileData.mot_de_passe && password != '' ? (
              <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
                <p style={{ color: 'red', marginTop: '-10px' }}>
                  Mot de passe incorrect
                </p>
              </div>
            ) : null}
          </div>
                      
           
            
            <div style={{ flexGrow: 1, marginRight: '10px' }}>
              <label htmlFor="new-password-input">Nouveau mot de passe</label><br/>
              <input
                type="password"
                id="new-password-input"
                value={newPassword}
                onChange={(event) => setNewPassword(event.target.value)}
              />
               {((/^\d+$/.test(newPassword))) || !(newPassword.length === 0) &&  (
        <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
          <p  style={{color: 'red',marginTop: '-10px'}}>Mot de passe doit être un entier</p>
        </div>
             )}
            </div>
            <div style={{ flexGrow: 1 }}>
              <label htmlFor="confirm-password-input">Confirmer mot de passe</label><br/>
              <input
                type="password"
                id="confirm-password-input"
                value={confirmPassword}
                onChange={(event) => setConfirmPassword(event.target.value)}
              />
               {confirmPassword !== newPassword && confirmPassword != '' && (
                  <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
                    <p style={{ color: 'red', marginTop: '-10px'}}>
                    Mots de passe ne correspondent pas
                    </p>
                  </div>
                )}
            </div>
          </div>
        </div>

        <button type="submit" className="submit-mdp" disabled={
  (!/^\d+$/.test(newPassword)) ||
    confirmPassword !== newPassword ||
    password === profileData.mot_de_passe ||
    password.length === 0  ||
    (newPassword.length === 0)
  }>
          Changer mot de passe
        </button>
        {successMessage && <p>{successMessage}</p>}
      </form>
    </div>
  );
};



export default Profil;