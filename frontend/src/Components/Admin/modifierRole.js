import React, { useState } from 'react';
import './ajouter.css'; 

const RoleForm = ({ user, handleClose }) => {
    const [roleName, setRoleName] = useState(user.role_name);
    const [rejeu, setRejeu] = useState(user.rejeu);
    const [frequentation, setFrequentation] = useState(user.frequentation);
    const [exploitation, setExploitation] = useState(user.exploitation);
    const [parcours, setParcours] = useState(user.parcours);
    const [suivi, setSuivi] = useState(user.suivi);
    const [successMessage, setSuccessMessage] = useState('');
  

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch(`/modifier_role/${roleName}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            rejeu: rejeu,
            frequentation: frequentation,
            exploitation: exploitation,
            parcours: parcours,
            suivi: suivi
          }),
        })
          .then(response => {
            // Traitez la réponse du serveur
            if (response.ok) {
              setSuccessMessage('Le rôle a été modifié avec succès!');
            } else {
              setSuccessMessage('Une erreur s\'est produite lors de la modification du rôle.');
            }
          })
          .catch(error => {
            // Gérez les erreurs
            setSuccessMessage("Une erreur s'est produite lors de la modification du rôle.");
          });
      };
  return (
    <div className="container">
      <h2>Modifier un rôle</h2>
      <br />
    <form  className="form-container"onSubmit={handleSubmit} >
      <label>
        Rôle
        <input type="text"  className="input-field" name="roleName" value={roleName} onChange={e => setRoleName(e.target.value)} />
      </label>
      <br /><br />
      <div className="checkbox-role">
      <input className="checkbox-role" type="checkbox" name="rejeu" checked={rejeu} onChange={e => setRejeu(e.target.checked)} />
       <label>
        Module rejeu
        </label>
      <br /><br />
      <input className="checkbox-role" type="checkbox" name="frequentation" checked={frequentation} onChange={e => setFrequentation(e.target.checked)} />
   
      <label>
        Module fréquentation
    </label>
      <br /><br />
      <input type="checkbox" name="exploitation" checked={exploitation} onChange={e => setExploitation(e.target.checked)} />
      <label>
        Module exploitation
        </label>
      <br /><br />
      
      <input  type="checkbox" name="parcours" checked={parcours} onChange={e => setParcours(e.target.checked)} />   
      <label>
        Module parcours
      </label>
      
      <br /><br />
      
      <input  type="checkbox" name="suivi" checked={suivi} onChange={e => setSuivi(e.target.checked)} />
  
      <label>
        Module suivi
      </label>
      </div>
    
      <br /><br />

      <button type="submit" className="submit-btn">Modifier un rôle</button>

      {successMessage && <p>{successMessage}</p>}
    </form>
    </div>
  );
};

export default RoleForm;