import React, { useState } from 'react';

const RoleForm = () => {
  const [roleName, setRoleName] = useState('');
  const [rejeu, setRejeu] = useState(false);
  const [frequentation, setFrequentation] = useState(false);
  const [exploitation, setExploitation] = useState(false);
  const [parcours, setParcours] = useState(false);
  const [suivi, setSuivi] = useState(false);
  const [successMessage, setSuccessMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch('/create_role', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        roleName: roleName,
        rejeu: rejeu,
        frequentation: frequentation,
        exploitation: exploitation,
        parcours: parcours,
        suivi: suivi
      }),
    })
      .then(response => {
        // Traitez la réponse du serveur
        if (response.ok) {
          setSuccessMessage('Le rôle a été créé avec succès!');
        } else {
          setSuccessMessage('Rôle existe déja.');
        }
      })
      .catch(error => {
        // Gérez les erreurs
        setSuccessMessage('Une erreur s\'est produite lors de la création du rôle.');
      });
  };

  return (
    <div className="container">
      <h2>Ajouter un rôle</h2>
      <br />
    <form onSubmit={handleSubmit} className="form-container">
      <label>
        Rôle
        <input type="text"  className="input-field" name="roleName" value={roleName} onChange={e => setRoleName(e.target.value)} />
      </label>
      <br /><br />
      <div className="checkbox-role">
      <input type="checkbox" name="rejeu" checked={rejeu} onChange={e => setRejeu(e.target.checked)} />
       <label>
        Module rejeu
        </label>
      <br /><br />
      <input type="checkbox" name="frequentation" checked={frequentation} onChange={e => setFrequentation(e.target.checked)} />
   
      <label>
        Module fréquentation
    </label>
      <br /><br />
      <input type="checkbox" name="exploitation" checked={exploitation} onChange={e => setExploitation(e.target.checked)} />
      <label>
        Module exploitation
        </label>
      <br /><br />
      <input type="checkbox" name="parcours" checked={parcours} onChange={e => setParcours(e.target.checked)} />   
      <label>
        Module parcours
      </label>
      <br /><br />
      <input type="checkbox" name="suivi" checked={suivi} onChange={e => setSuivi(e.target.checked)} />
  
      <label>
        Module suivi
            </label>
            </div>
      <br /><br />

      <button type="submit" className="submit-btn">Ajouter un rôle</button>

      {successMessage && <p>{successMessage}</p>}
    </form>
    </div>
  );
};

export default RoleForm;