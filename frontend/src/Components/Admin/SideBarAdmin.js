import React, { useState } from 'react';
import { FaUser, FaUsers,FaUserCog , FaUserPlus,FaSignOutAlt } from 'react-icons/fa';
import './SideBarAdmin.css';
import { MdPersonAdd } from 'react-icons/md';
import Logo from '../../assets/logo_sideBar.png';
import { Link } from 'react-router-dom';
import axios from 'axios';

function SideBarAdmin(props) {
  const [darkMode, setDarkMode] = useState(false);
  const [activeIcon, setActiveIcon] = useState(null);

  const handleIconClick = (icon) => {
    setActiveIcon(icon);
    props.onIconClick(icon);
  };

  const handleLogout = () => {
    axios.post('/logout')
      .then(response => {
        // Succès de la déconnexion, effectuez les actions nécessaires (redirection, etc.)
      })
      .catch(error => {
        // Gérer les erreurs de requête
      });
  };

 

  return (
    <div className={darkMode ? 'sidebar-container dark-mode' : 'sidebar-container'}>
      <div className={darkMode ? 'sidebar dark-mode' : 'sidebar'}>
      <div className="sidebar-icons">
        <div className="sidebar-icon logo-icon">
        <img src={Logo} alt="logo" class="logo"/>
        </div>
        <br/>
        <div className="sidebar-icons">
          <div className={`sidebar-icon ${activeIcon === 'profil' ? 'active' : ''}`} onClick={() => handleIconClick('profil')}>
            <FaUser />
            <span>Profil</span>
          </div>
          
          <div className={`sidebar-icon ${activeIcon === 'roles' ? 'active' : ''}`} onClick={() => handleIconClick('roles')}>
            <FaUserCog  />
            <span>Rôles</span>
          </div>
          <div className={`sidebar-icon ${activeIcon === 'utilisateurs' ? 'active' : ''}`} onClick={() => handleIconClick('utilisateurs')}>
            <FaUsers />
            <span>Comptes</span>
          </div>
          <div className={`sidebar-icon ${activeIcon === 'conducteurs' ? 'active' : ''}`} onClick={() => handleIconClick('conducteurs')}>
            <FaUsers />
            <span>Conducteurs</span>
          </div>

          
         </div>
         <Link to="/" className="sidebar-icon logout-icon">
  <FaSignOutAlt />
  <span>Déconnexion</span>
</Link>
          
          
        </div>
      </div>

      </div>

  );
};

export default SideBarAdmin;
