import React, {  useState ,useEffect} from 'react';
import axios from 'axios';
import { FaUser, FaEnvelope, FaUsers, FaIdCard, FaLock } from 'react-icons/fa';
import './ajouter.css';


const Ajouter = () => {
  const [nom, setNom] = useState('');
  const [role, setRole] = useState('');
  const [email, setEmail] = useState('');
  const [matricule, setMatricule] = useState('');
  const [message, setMessage] = useState('');
  const [addMessage, setAddMessage] = useState('');
  const [options, setOptions] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('http://localhost:5000/afficherRole', {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      });
      setOptions(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  
  const handleSubmit = async (e) => {
    
    e.preventDefault();
  
    
    // Send the data to the backend to register the user
    try {
      const response = await fetch('/AjouterConducteur', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          nom_prenom: nom,
          email: email,
          matricule: matricule,
          role_compte: role,
        }),
      });
  
      const data = await response.json();
      console.log('Response:', data);
      // Handle the response from the backend
      if (data.success) {
        setAddMessage('Le conducteur a été ajouté avec succès!');
        
      } else {
        setMessage(data.message);
   
      }
    } catch (error) {
      console.error('Error:', error);
      // Handle any network or server errors
    }
    
  };

  
  



  return (
    <div>
    <div className="container-ajouter">
      <h2>Ajouter un conducteur</h2>
      <br></br>
      <form onSubmit={handleSubmit} className="form-container">
     
       
      <label htmlFor="matricule">
          Matricule
          <div class="input-container">
          <FaIdCard className='icon'/>
          <input
            id="matricule"
            type="text"
            value={matricule}
            onChange={(e) => setMatricule(e.target.value)}
            className="input-field"
            required
          />
          </div>
         
        </label>
        {((matricule.length === 5 && /^\d+$/.test(matricule))) || !(matricule.length === 0) &&  (
        <div className="alert" style={{ backgroundColor: 'white', color: 'black' }}>
          <p  style={{color: 'red',marginTop: '-10px'}}>Matricule invalide</p>
        </div>
      )}
      <br/>
        <label htmlFor="nom">
          Nom prénom
          <div class="input-container">
          <FaUser className='icon'/>
          <input
            id="nom"
            type="text"
            value={nom}
            onChange={(e) => setNom(e.target.value)}
            className="input-field"
            required
          />
          </div>
          
        </label>
       
<br/> <br/>
       
        <button type="submit" className="submit-btn">
          Ajouter un conducteur
        </button>
        {addMessage && <b  style={{color: '#18B1AC'}}>{addMessage}</b>}
        {message && <p  style={{color: 'red'}}>{message}</p>}
        
      </form>
    </div>
    </div>
   
  );
};

export default Ajouter;
