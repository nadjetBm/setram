import React, { useState, useEffect } from "react";
import axios from "axios";
import Chart from "react-apexcharts";

const FrequentationNbValJour = ({ date, setDate, selectedVoie}) => {
const [data, setData] = useState({});
const [validationData, setValidationData] = useState([]);
const [horaireData, setHoraireData] = useState([]);


useEffect(() => {
const fetchData = async () => {
const result = await axios.get(
`http://localhost:5000/progress-bar-data/${date}/${selectedVoie}`
);
setData(result.data);
  const validationResult = await axios.get(
    `http://localhost:5000/validation-data/${date}/${selectedVoie}`
  );
setValidationData(validationResult.data);

const HoraireResult = await axios.get(
  `http://localhost:5000/tranche-data/${date}/${selectedVoie}`
);
setHoraireData(HoraireResult.data);

};

fetchData();
}, [date, selectedVoie]);

const progressBarOptions = {
chart: {
id: "ValidationsJour",
type: "bar",
height: 100,
},
plotOptions: {
bar: {
horizontal: true,
barHeight: "100%",
},
},
dataLabels: {
enabled: true,
},
xaxis: {
categories: ["Nombre de validations"],
labels: {
show: false,
},
},
yaxis: {
max: 50000,
labels: {
show: false,
},
},
fill: {
colors: ["#18B1AC"],
},
};

const progressBarSeries = [
{
name: "Nombre de validations",
data: [data.nombre_validation || 0],
},
];

const validationChartOptions = {
chart: {
id: "ValidationsStationJour",
type: "bar",
height: 400,
},
plotOptions: {
bar: {
horizontal: false,
columnWidth: "30%",
},
},
dataLabels: {
enabled: false,
},
xaxis: {
title: {
  text: "Station de validations",
},
categories: validationData.map(data => data.station_validation),
labels: {
show: true,

},
},
yaxis: {
title: {
text: "Nombre de validations",
},
},
fill: {
colors: ["#18B1AC"],
},
};

const validationChartSeries = [
{
name: "Nombre de validations",
data: validationData.map(data => data.nombre_validation),
},
];
const horaireChartOptions = {
  chart: {
  id: "ValidationTrancheHoraireJour",
  type: "line",
  height: 400,
  },
  plotOptions: {
  line: {
  horizontal: false,
  columnWidth: "50%",
  },
  },
  dataLabels: {
  enabled: false,
  },
  xaxis: {
  title: {
    text: "Tranche horaire de validations",
  },
  categories: horaireData.map(data => data.tranche_horaire),
  labels: {
  show: true,
  },
  },
  yaxis: {
  title: {
  text: "Nombre de validations",
  },
  },
  colors: ["#18B1AC"],
  };
  
  const horaireChartSeries = [
  {
  name: "Nombre de validations",
  data: horaireData.map(data => data.nombre_validation),
  },
  ];

return (
<div>
<div className="container-bar">
<h3 style={{color: '#0f2b4a'}}>Nombre de validations par jour</h3>
<p>Nombre de validations : {data.nombre_validation || 0} validations en entrée</p>
    <Chart
      options={progressBarOptions}
      series={progressBarSeries}
      type="bar"
      height={100}
      width={500}
    />
  </div>
  <div className="container-bar">
    <h3 style={{color: '#0f2b4a'}}>Nombre de validations par station</h3>
    <Chart
      options={validationChartOptions}
      series={validationChartSeries}
      type="bar"
      height={450}
      width={700}
    />
  </div>
  <div className="container-bar">
    <h3 style={{color: '#0f2b4a'}}>Nombre de validations par tranche horaire</h3>
    <Chart
      options={horaireChartOptions}
      series={horaireChartSeries}
      type="line"
      height={450}
      width={700}
    />
  </div>
</div>
);
};

export default FrequentationNbValJour;