import './SideBarFrequentation.css'
import SideBarFrequentation from "./SideBarFrequentation";
import React, { useState, useEffect } from 'react';
import axios from 'axios'; 


function Frequentation(){

   
    const [frequentation, setFrequentation] = useState(false);
   
  
      
      useEffect(() => {
        fetchRoleData();
      }, []);
      
      const fetchRoleData = async () => {
        try {
          const response = await axios.get('/display_components');
          const {frequentation } = response.data;
          setFrequentation(frequentation);
         
      
        } catch (error) {
          console.error('Error fetching role data:', error);
        }
      };
  
    return(
        <React.Fragment>
        
            <React.Fragment>
              <SideBarFrequentation/>
            </React.Fragment>
         
        </React.Fragment>
        
    )
}
export default  Frequentation;