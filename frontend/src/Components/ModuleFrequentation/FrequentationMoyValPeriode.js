import React, { useState, useEffect } from "react";
import axios from "axios";
import Chart from "react-apexcharts";

const FrequentationMoyValPeriode = ({ startDate, setStartDate,endDate, setEndDate, selectedVoie}) => {
const [data, setData] = useState({});
const [validationData, setValidationData] = useState([]);
const [horaireData, setHoraireData] = useState([]);


useEffect(() => {
const fetchData = async () => {
const result = await axios.get(
`http://localhost:5000/moyPeriode-progress/${startDate}/${endDate}/${selectedVoie}`
);
setData(result.data);
  const validationResult = await axios.get(
    `http://localhost:5000/moyPeriode-bar/${startDate}/${endDate}/${selectedVoie}`
  );
setValidationData(validationResult.data);

const HoraireResult = await axios.get(
  `http://localhost:5000/moyPeriode-line/${startDate}/${endDate}/${selectedVoie}`
);
setHoraireData(HoraireResult.data);

};

fetchData();
}, [startDate,endDate, selectedVoie]);

const progressBarOptions = {
chart: {
id: "MoyenneValidationsPeriode",
type: "bar",
height: 100,
},
plotOptions: {
bar: {
horizontal: true,
barHeight: "100%",
},
},
dataLabels: {
enabled: true,
},
xaxis: {
categories: ["Moyenne de validations"],
labels: {
show: false,
},
},
yaxis: {
max: 500,
labels: {
show: false,
},
},
fill: {
colors: ["#18B1AC"],
},
};

const progressBarSeries = [
{
name: "Moyenne  de validations",
data: [Math.round(data.nombre_validation)],
},
];

const validationChartOptions = {
chart: {
id: "MoyenneValidationsStationPeriode",
type: "bar",
height: 400,
},
plotOptions: {
bar: {
horizontal: false,
columnWidth: "30%",
},
},
dataLabels: {
enabled: false,
},
xaxis: {
  title: {
    text: "Station de validations",
  },
categories: validationData.map(data => data.station_validation),
labels: {
show: true,
},
},
yaxis: {
title: {
text: "Moyenne  de validations",
},
},
fill: {
colors: ["#18B1AC"],
},
};

const validationChartSeries = [
{
name: "Moyenne de validations",
data: validationData.map(data => Math.round(data.nombre_validation)),
},
];
const horaireChartOptions = {
  chart: {
  id: "MoyenneValidationTrancheHorairePeriode",
  type: "line",
  height: 400,
  },
  plotOptions: {
  line: {
  horizontal: false,
  columnWidth: "50%",
  },
  },
  dataLabels: {
  enabled: false,
  },
  xaxis: {
  title: {
  text: "Tranche horaire de validations",
  },
  categories: horaireData.map(data => data.tranche_horaire),
  labels: {
  show: true,
  },
  },
  yaxis: {
  title: {
  text: "Moyenne  de validations",
  },
  },
  colors: ["#18B1AC"],
  };
  
  const horaireChartSeries = [
  {
  name: "Moyenne de validations",
  data: horaireData.map(data => Math.round(data.nombre_validation)),
  },
  ];

return (
<div>
<div className="container-bar">
<h3 style={{color: '#0f2b4a'}} >Moyenne de validations des trams par période</h3>
<p>Moyenne de validations : {Math.round(data.nombre_validation || 0)} validations en entrée</p>
    <Chart
      options={progressBarOptions}
      series={progressBarSeries}
      type="bar"
      height={100}
      width={500}
    />
  </div>
  <div className="container-bar">
    <h3 style={{color: '#0f2b4a'}} >Moyenne de validations par station</h3>
    <Chart
      options={validationChartOptions}
      series={validationChartSeries}
      type="bar"
      height={450}
      width={700}
    />
  </div>
  <div className="container-bar">
    <h3 style={{color: '#0f2b4a'}}>Moyenne de validations par tranche horaire</h3>
    <Chart
      options={horaireChartOptions}
      series={horaireChartSeries}
      type="line"
      height={450}
      width={700}
    />
  </div>
</div>
);
};

export default FrequentationMoyValPeriode;