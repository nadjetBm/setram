import ProgressBar from "./FrequentationNbValJour";
import NbValsemaine from "./FrequentationNbValPeriode";
import MoyValSemaine from "./FrequentationMoyValPeriode";
import MoyValJour from "./FrequentationMoyValJour";
import SansIndicateur from "../Common/SansIndicateurs"
import './SideBarFrequentation.css'
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import { FaFileCsv} from 'react-icons/fa';
import moment from 'moment';
import React, { useState, useRef } from "react";
import {FaImage} from 'react-icons/fa';
import html2canvas from 'html2canvas';



const SidBarFrequentation = () => {

  const [date, setDate] = useState('');
  const [showProgressBar, setShowProgressBar] = useState(false);
  const [showMoyalibComponent, setShowMoyalibComponent] = useState(false);
  const [showSansIndicateur, setShowSansIndicateur] = useState(true);
  const [selectedVoie, setSelectedVoie] = useState("");
  
  
  const handleChange = (event) => {
    setDate(event.target.value);
    setStartDate("");
    setEndDate("");
  };



  const handleProgressBarChange = (event) => {
    setShowProgressBar(event.target.checked);
    setShowMoyalibComponent(false); // désélectionne l'autre case
    setShowSansIndicateur(!(event.target.checked || showMoyalibComponent));
  };
  
  const handleDatesChange = ({ startDate, endDate }) => {
    setStartDate(startDate);
    setEndDate(endDate);
    setDate("");
  };
  
  const handleMoyalibComponentChange = (event) => {
    setShowMoyalibComponent(event.target.checked);
    setShowProgressBar(false); // désélectionne l'autre case
    setShowSansIndicateur(!(event.target.checked || showMoyalibComponent ));
  };
 
  const containerRef = useRef(null);
  const saveContainerAsImage = () => {
    html2canvas(containerRef.current).then((canvas) => {
      const image = canvas.toDataURL('image/png'); 
      const link = document.createElement('a');
      link.href = image;
      link.download = `${date}||${startDate}-${endDate}.png`; 
      link.click();
    });
  };
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [focusedInput, setFocusedInput] = useState(null);


  const handleVoieChange = (event) => {
    setSelectedVoie(event.target.value);
  };

  return (
    <div className="parent-container">
      <div className="left-container">
        <h3 style={{color: '#0f2b4a'}}>Module fréquentation</h3><br/>
        <p style={{color: '#0f2b4a'}}><b>Paramètres temporels</b></p>
        <p style={{color: '#0f2b4a'}}>Date</p>
        <input type="date" value={date} onChange={handleChange} />
       
        <p style={{color: '#0f2b4a'}}>Période</p>
      <div className="datePicker"> 
      <DateRangePicker
        startDate={startDate}
        endDate={endDate}
        onDatesChange={handleDatesChange}
        focusedInput={focusedInput}
        onFocusChange={focusedInput => setFocusedInput(focusedInput)}
        startDatePlaceholderText="Date début -"
        endDatePlaceholderText="Date fin"
        minimumNights={0}
        small={true}
        isOutsideRange={(day) => day.isAfter(moment(), 'day')}
       
      />
      </div>
      <br></br>
        <p style={{color: '#0f2b4a'}}><b>Paramètre spatial</b></p>
        
        <select className="select" value={selectedVoie} onChange={handleVoieChange}>
        <option value="" selected disabled>Voie</option>
          <option value="1">Voie 1</option>
          <option value="2">Voie 2</option>
        </select>
        <br/> <br/>
        <p style={{color: '#0f2b4a'}}><b>Agrégation</b></p>
        <label>
          <input  type="checkbox" checked={showProgressBar} onChange={handleProgressBarChange} />
          Nombre de validations
        </label>
        <label>
          <br/>
          <input type="checkbox" checked={showMoyalibComponent} onChange={handleMoyalibComponentChange} />
          Moyenne de validations
        </label>
        <button className="export-button" onClick={saveContainerAsImage}><i className="fas fa-file-export"></i>Capture &nbsp;<FaImage/></button>
      </div>
      <div className="right-container" ref={containerRef}>
      {!showProgressBar && !showMoyalibComponent  && showSansIndicateur && <SansIndicateur />}
        {selectedVoie && date && showProgressBar && <ProgressBar date={date} setDate={setDate} selectedVoie ={selectedVoie} setSelectedVoie={setSelectedVoie}/>}
        {selectedVoie && date && showMoyalibComponent && <MoyValJour date={date} setDate={setDate} selectedVoie ={selectedVoie} setSelectedVoie={setSelectedVoie}/>}
        {selectedVoie && startDate && showMoyalibComponent && <MoyValSemaine startDate={startDate} endDate={endDate} selectedVoie ={selectedVoie} setSelectedVoie={setSelectedVoie}/>}
        {selectedVoie && startDate && showProgressBar && <NbValsemaine startDate={startDate} endDate={endDate} selectedVoie ={selectedVoie} setSelectedVoie={setSelectedVoie}/>}

        
      </div>
    </div>
  );
};

export default SidBarFrequentation;