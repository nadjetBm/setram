
import ParcoursRetardDepartJour from "./ParcoursRetardDepart";
import ParcoursRetardArriveJour from "./ParcoursRetardArrive";
import html2canvas from 'html2canvas'
import {FaImage} from 'react-icons/fa';
import SansIndicateur from "../Common/SansIndicateurs"
import './SideBarParcours.css'
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import moment from 'moment';
import React, { useState, useRef } from "react";




const SideBarParcours = () => {

  const [date, setDate] = useState('');
  const [showParcoursRetard, setShowParcoursRetard] = useState(false);
  const [showParcoursRetardArrive, setShowParcoursRetardArrive] = useState(false);
  const [showParcoursRetardDepart, setShowParcoursRetardDepart] = useState(false);
  const [showSansIndicateur, setShowSansIndicateur] = useState(true);
  const containerRef = useRef(null);
  const saveContainerAsImage = () => {
    html2canvas(containerRef.current).then((canvas) => {
      const image = canvas.toDataURL('image/png'); // Convert canvas to base64 image URL
      const link = document.createElement('a');
      link.href = image;
      link.download =`${date}||${startDate}-${endDate}.png`; // Set the downloaded file name
      link.click();
    });
  };
  
  


  const handleParcoursRetardChange = (event) => {
    setShowParcoursRetard(event.target.checked);
    setShowParcoursRetardArrive(event.target.checked);
    setShowParcoursRetardDepart(event.target.checked);
    setShowParcoursRetardDepart(false); // désélectionne l'autre case
    setShowParcoursRetardArrive(false); // désélectionne l'autre case
    setShowSansIndicateur(!(event.target.checked));
  };
  const handleParcoursRetardArriveChange = (event) => {
    setShowParcoursRetardArrive(event.target.checked);
    setShowParcoursRetardDepart(false); // désélectionne l'autre case
    setShowSansIndicateur(!(event.target.checked));
  };

  
  const handleParcoursRetardDepartChange = (event) => {
    setShowParcoursRetardDepart(event.target.checked);
    setShowParcoursRetardArrive(false); // désélectionne l'autre case// désélectionne l'autre case
    setShowSansIndicateur(!(event.target.checked));
  };


  const handleChange = (event) => {
    setDate(event.target.value);
    setStartDate("");
    setEndDate("");
  };

  
  const handleDatesChange = ({ startDate, endDate }) => {
    setStartDate(startDate);
    setEndDate(endDate);
    setDate("");
  };

  const handleExport = () => {
    // TODO: Ajouter la logique pour exporter les données
    console.log('Exporter les données');
  };
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [focusedInput, setFocusedInput] = useState(null);
  const [selectedHour, setSelectedHour] = useState("");
  const [selectedVoie, setSelectedVoie] = useState("");

  const handleVoieChange = (event) => {
    setSelectedVoie(event.target.value);
  };

  const hours= Array.from({ length: 19 }, (_, index) => index + 5);

  return (
    <div className="parent-container">
      <div className="left-container">
        <h3 style={{color: '#0f2b4a'}}>Module parcours</h3><br/>
        <p style={{color: '#0f2b4a'}}><b>Paramètres temporels</b></p>
        <p style={{color: '#0f2b4a'}}>Date</p>
        <input type="date" value={date} onChange={handleChange} />
       <br/>
        <select className="select" value={selectedHour} onChange={(event) => setSelectedHour(event.target.value)}>
          <option value="" selected disabled>Tranche horaire</option>
          {hours.map((hour) => (
            <option key={hour} value={hour}>{hour}</option>
          ))}
        </select>
        <br/>
        <p style={{color: '#0f2b4a'}}><b>Paramètre spatial</b></p>
        
        <select value={selectedVoie} onChange={handleVoieChange} className="select">
        <option value="" selected disabled>Voie</option>
          <option value="1">voie 1</option>
          <option value="2">voie 2</option>
        </select>

      <div className="datePicker"> 
     <br/>
      </div>
      <p style={{color: '#0f2b4a'}}><b>Indicateurs</b></p>
        <label>
          <input  type="checkbox" checked={showParcoursRetard} onChange={handleParcoursRetardChange} />
          Retard et avance
        </label>
        <br/>
        <p style={{color: '#0f2b4a'}}><b>Agrégation</b></p>
        <label>
          
          <input type="checkbox" checked={showParcoursRetardArrive} onChange={handleParcoursRetardArriveChange}  />
          A l'arrivée
        </label>
        <label>
        <br/>
          <input  type="checkbox" checked={showParcoursRetardDepart} onChange={handleParcoursRetardDepartChange}   />
          Au départ
        </label>
       
        <button className="export-button" onClick={saveContainerAsImage}><i className="fas fa-file-export"></i>Capture &nbsp;<FaImage/></button>
   </div>
      <div className="right-container"  ref={containerRef}>
      {!showParcoursRetardArrive && !showParcoursRetardDepart  && showSansIndicateur && <SansIndicateur />}
        {selectedVoie && selectedHour && date && showParcoursRetardArrive && showParcoursRetard && <ParcoursRetardArriveJour date={date} setDate={setDate} selectedHour={selectedHour} setSelectedHour={setSelectedHour} selectedVoie ={selectedVoie} setSelectedVoie={setSelectedVoie}/>}
        {selectedVoie && selectedHour && date && showParcoursRetardDepart && showParcoursRetard && <ParcoursRetardDepartJour date={date} setDate={setDate} selectedHour={selectedHour} setSelectedHour={setSelectedHour} selectedVoie ={selectedVoie} setSelectedVoie={setSelectedVoie} />}
             
      </div>
    </div>
  );
};


export default SideBarParcours;