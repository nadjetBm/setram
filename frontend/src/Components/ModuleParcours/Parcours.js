import React, { useState, useEffect } from 'react';
import axios from 'axios';
import{Routes,Route} from 'react-router-dom' 
import SansIndicateurs from '../Common/SansIndicateurs'
import SideBarParcours from './SideBarParcours'
import PasAcces from '../Common/PasAcces'
function Parcours()
{
    const [parcours, setParcours] = useState(false);
    
    useEffect(() => {
      fetchRoleData();
    }, []);
    
    const fetchRoleData = async () => {
      try {
        const response = await axios.get('/display_components');
        const {parcours} = response.data;
        setParcours(parcours);
      } catch (error) {
        console.error('Error fetching role data:', error);
      }
    };
    return(
        <React.Fragment>
        
            <React.Fragment>
              <SideBarParcours />
            </React.Fragment>
         
                </React.Fragment>
    )
}
export default Parcours;