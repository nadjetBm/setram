import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import axios from 'axios';
import { FaMapMarker } from 'react-icons/fa';
import { FaTram } from 'react-icons/fa';
import { AiOutlineClose } from 'react-icons/ai';

function TramConducteurComponent({ date, setDate, selectedHour, setSelectedHour, selectedVoie }) {
  const [data, setData] = useState([]);
  const [stations, setStations] = useState([]);
  const [selectedRowIndex, setSelectedRowIndex] = useState(null);
  const [selectedColumnIndex, setSelectedColumnIndex] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/tramconducteurArrive/${date}/${selectedHour}/${selectedVoie}`);
        const data = response.data;
        setStations(data.stations.filter(station => station));
        setData(data.data);
      } catch (error) {
        console.error('Error:', error);
      }
    };
    fetchData();
  }, [date, selectedHour, selectedVoie]);


  const handleCellClick = (rowIndex, columnIndex) => {
    setSelectedRowIndex(rowIndex);
    setSelectedColumnIndex(columnIndex);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const selectedRow = data[selectedRowIndex];
  const selectedStation = stations[selectedColumnIndex];

  const customModalStyles = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    content: {
      width: '400px',
      margin: '0 auto',
      borderRadius: '4px',
      padding: '20px',
    },
  };
  const closeButtonStyle = {
    position: 'absolute',
    top: '10px',
    right: '10px',
    border: 'none',
    background: 'none',
    cursor: 'pointer',
  };
 
    
  const etatMapping = {
    'COM': 'Commercial',
    'HLP': 'Sans voyageurs'
  }

  const stationMapping = {
    'JARD1': 'jardin public1',
    'FACU1': 'Faculté de Droit1',
    'SIDJ1': 'Sidi Djillali1',
    'WIAM1': 'Place El Wiam1',
    'MATE1': 'Maternité1',
    'HAMO1': 'Ben Hamouda1',
    'NGFE1': 'Gare Ferroviaire1',
    'RADI1': 'Radio1',
    'ADNA1': 'Les Frères Adnane1',
    'NIAA1': 'Centre En Niaama1',
    'HORL1': 'Quatre Horloges1',
    'GRES1': 'Gare Routière Est1',
    'CASC1': 'Les Cascades1',
    'GRSU1': 'Gare Routière Sud1',
    'CAMP1': 'Campus Universitaire1',
    'GRNO1': 'Gare Routière Nord1',
    'CITE1': 'Cité H. Boumedienne1',
    'ADAB1': 'Adda Boudjella1l',
    'ENVI1': 'Environnement1',
    'EABD1': 'Emir Abdelkader1',
    'DAIR1': 'La Daira1',
    'JARD2': 'jardin public2',
    'FACU2': 'Faculté de Droit2',
    'SIDJ2': 'Sidi Djillali2',
    'WIAM2': 'Place El Wiam2',
    'MATE2': 'Maternité2',
    'HAMO2': 'Ben Hamouda2',
    'NGFE2': 'Gare Ferroviaire2',
    'RADI2': 'Radio2',
    'ADNA2': 'Les Frères Adnane2',
    'NIAA2': 'Centre En Niaama2',
    'HORL2': 'Quatre Horloges2',
    'GRES2': 'Gare Routière Est2',
    'CASC2': 'Les Cascades2',
    'GRSU2': 'Gare Routière Sud2',
    'CAMP2': 'Campus Universitaire2',
    'GRNO2': 'Gare Routière Nord2',
    'CITE2': 'Cité H. Boumedienne2',
    'ADAB2': 'Adda Boudjellal2',
    'ENVI2': 'Environnement2',
    'EABD2': 'Emir Abdelkader2',
    'DAIR2': 'La Daira2'
  };
  const orderedStations = ['GRSU1','JARD1','HORL1','EABD1','ADAB1','MATE1','RADI1','CITE1','DAIR1','WIAM1','SIDJ1','AADL1',
 'GRNO1','NGFE1','CAMP1','NIAA1','FACU1','ENVI1','HAMO1','ADNA1','GRES1','CASC1','CASC2', 'GRES2', 'ADNA2', 'HAMO2', 'ENVI2',
 'FACU2', 'NIAA2', 'CAMP2', 'NGFE2', 'GRNO2', 'AADL2', 'SIDJ2', 'WIAM2', 'DAIR2', 'CITE2', 'RADI2', 'MATE2', 'ADAB2', 'EABD2', 'HORL2', 'JARD2', 'GRSU2'];

const sortedStations = stations.sort((a, b) => {
  const indexA = orderedStations.indexOf(a);
  const indexB = orderedStations.indexOf(b);
  
  if (indexA < indexB) {
    return -1;
  } else if (indexA > indexB) {
    return 1;
  }
  
  return 0;
});


  return (
    <div>
      <br />
      <h2 style={{ fontSize: '22px' ,color: '#333' }}>Parcours des trams</h2>
      <div style={{ textAlign: 'center' }}>
    <span style={{ marginRight: '30px' ,color: '#666'}}>
      <FaTram style={{ color: 'red', fontSize: '20px' }} />
      &nbsp; Avance
    </span>
    <span style={{ marginRight: '30px' ,color: '#666'}}>
      <FaTram style={{ color: '#18B1AC', fontSize: '20px' }} />
      &nbsp; Retard
    </span>
    <span style={{ marginRight: '30px' ,color: '#666'}}>
      <FaTram style={{ color: '#FFA500', fontSize: '20px' }} />
      &nbsp; À l'heure
    </span>
  </div>
  <br></br>
     
      <table style={{ margin: '0 auto', borderCollapse: 'collapse' }}>
        <thead>
          <tr>
            <th style={{ padding: '5px', border: '1px solid #ccc' }}></th>
            {sortedStations.map((station, index) => (
              <th key={index} style={{ padding: '5px', border: '1px solid #ccc', textAlign: 'center' }}>
                <span style={{ fontSize: '12px', color: '#666' }}>{station}</span>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((row, rowIndex) => (
            <tr key={rowIndex}>
              <td style={{ padding: '5px', border: '1px solid #ccc' }}>
                <b style={{ color: '#0f2b4a', fontSize: '14px', marginRight: '10px', marginLeft: '10px' }}>{row.heure}</b>
              </td>
              {stations.map((station, columnIndex) => {
                const isStationMatched = row.station === station;
                const isSelected =
                  rowIndex === selectedRowIndex && columnIndex === selectedColumnIndex;
                let cellContent;

                if (isStationMatched) {
                  if (row.retard_arrive !== null) {
                    cellContent = (
                      <td
                        key={columnIndex}
                        onClick={() => handleCellClick(rowIndex, columnIndex)}
                        className={isSelected ? 'selected' : ''}
                        style={{ padding: '5px', border: '1px solid #ccc', textAlign: 'center' }}
                      >
                        <FaTram
                          style={{
                            color: '#18B1AC',
                            fontSize: '20px',
                          }}
                        />
                      </td>
                    );
                  } else if (row.avance_arrive !== null) {
                    cellContent = (
                      <td
                        key={columnIndex}
                        onClick={() => handleCellClick(rowIndex, columnIndex)}
                        className={isSelected ? 'selected' : ''}
                        style={{ padding: '5px', border: '1px solid #ccc', textAlign: 'center' }}
                      >
                        <FaTram
                          style={{
                            color: 'red',
                            fontSize: '20px',
                          }}
                        />
                      </td>
                    );
                  } else {
                    cellContent = (
                      <td
                        key={columnIndex}
                        onClick={() => handleCellClick(rowIndex, columnIndex)}
                        className={isSelected ? 'selected' : ''}
                        style={{ padding: '5px', border: '1px solid #ccc', textAlign: 'center' }}
                      >
                        <FaTram
                          style={{
                            color: '#FFA500',
                            fontSize: '20px',
                          }}
                        />
                      </td>
                    );
                  }
                } else {
                  cellContent = (
                    <td key={columnIndex} style={{ padding: '5px', border: '1px solid #ccc', textAlign: 'center' }}>
                      <FaMapMarker
                        style={{
                          color: '#ccc',
                          fontSize: '16px',
                        }}
                      />
                    </td>
                  );
                }

                return cellContent;
              })}
            </tr>
          ))}
        </tbody>
      </table>

      <Modal isOpen={isModalOpen} onRequestClose={closeModal} style={customModalStyles}>
        {selectedRow && selectedStation && (
          <div>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Nom Prénom:</strong> {selectedRow.nom_prenom}
            </p>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Matricule agent:</strong> {selectedRow.matricule_agent}
            </p>
            <br />
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Station:</strong> {stationMapping[selectedRow.station] || selectedRow.station}
            </p>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Numéro tram:</strong> {selectedRow.tram}
            </p>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Etat:</strong> {etatMapping[selectedRow.etat] || selectedRow.etat}
            </p>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Service véhicule:</strong> {selectedRow.service_vehicule}
            </p>
            <br />
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Arrivée théorique:</strong> {selectedRow.arrive_theorique}
            </p>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Arrivée applicable:</strong> {selectedRow.arrive_applicable}
            </p>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Arrivée réel:</strong> {selectedRow.arrive_reel}
            </p>
            <br />
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Temps arrêt théorique:</strong> {selectedRow.temps_arret_theorique}
            </p>
            <p style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Temps arrêt applicable:</strong> {selectedRow.temps_arret_applicable}
            </p>
            <p  style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Temps arrêt réel:</strong> {selectedRow.temps_arret_reel}
            </p>
            <br />
            <p  style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Retard à l'arrivée:</strong> {selectedRow.retard_arrive !== null ? selectedRow.retard_arrive : '00:00:00'}
            </p>
            <p  style={{ fontSize: '14px', color: '#0f2b4a' }}>
              <strong>Avance à l'arrivée:</strong> {selectedRow.avance_arrive !== null ? selectedRow.avance_arrive : '00:00:00'}
            </p>
          </div>
        )}
        <button onClick={closeModal} style={closeButtonStyle}>
    <AiOutlineClose style={{ fontSize: '20px' }} />
  </button>
      </Modal>
    </div>
  );
}

export default TramConducteurComponent;
