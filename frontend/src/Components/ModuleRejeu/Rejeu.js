import React, { useState, useEffect } from 'react';
import axios from 'axios';
import SideBarRejeu from './SideBarRejeu';
import './Rejeu.css';
import PasAcces from '../Common/PasAcces'

function App() {
  const [rejeu, setRejeu] = useState(false);
    
    useEffect(() => {
      fetchRoleData();
    }, []);
    
    const fetchRoleData = async () => {
      try {
        const response = await axios.get('/display_components');
        const { rejeu} = response.data;
        setRejeu(rejeu);
      } catch (error) {
        console.error('Error fetching role data:', error);
      }
    };
  const handleFormSubmit = (selectedDate) => {
    fetch('/api/rejeu', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        date: selectedDate,
      })
    })
      .then(response => response.json())
      .then(data => {
       // afficher la carte
      })
      .catch(error => {
        console.error('Error:', error);
      });
  }

  return (
    <>
    
   
    
    <div className="container-rejeu">
     <div className="sidebar-rejeu">
        <SideBarRejeu onSubmit={handleFormSubmit} />
        
      </div>
  
        <div className="iframe-container">
          <iframe src="/rejeu.html" title="Carte des trams"></iframe>
        </div>
        </div>  
 
      
     
    
    </>
  );
}

export default App;