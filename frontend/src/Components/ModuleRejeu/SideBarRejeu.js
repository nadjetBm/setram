import React, { useState } from 'react';
import './SideBarRejeu.css'


function SideBarRejeu(props) {
  
  const [selectedDate, setSelectedDate] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault(); // empêcher le rafraîchissement de la page
    props.onSubmit(selectedDate); // envoyer la date et la voie sélectionnées à la fonction onSubmit de l'appelant
  }
  
  
  return (
    <div className='sidebarrejeu'>
       <h3 style={{color: '#0f2b4a'}}>Module rejeu</h3><br/>
       <p style={{color: '#0f2b4a'}}><b>Paramètre temporel</b></p>
      <p style={{color: '#0f2b4a'}}>Date</p>
   
      <form onSubmit={handleSubmit}>

        <input type="date" id="date" name="date" value={selectedDate} onChange={e => setSelectedDate(e.target.value)} />

        <input type="submit"  className="submit-rejeu" value="Afficher la carte" disabled={!selectedDate} />
      </form>
      <br />
    </div>
   


  )
}

export default SideBarRejeu;