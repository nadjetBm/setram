import React, { useState } from 'react';
import './login.css';
import { FaUser,FaLock } from 'react-icons/fa';
import imageSrc from './assets/lignesba.png';
import Logo from './assets/logo.jpg'

function Login() {
  const [nomPrenom, setNomPrenom] = useState('');
  const [motDePasse, setMotDePasse] = useState('');
  const [message, setMessage] = useState('');

  const handleLogin = async () => {
    const response = await fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        nom_prenom: nomPrenom,
        mot_de_passe: motDePasse,
      }),
    });

    const data = await response.json();

    if (response.ok) {
      setMessage(data.message);
      if (data.redirect) {
        window.location.href = data.redirect;
      } else {
        // Rediriger vers une autre page par défaut si aucune redirection spécifiée
        window.location.href = '/';
      }
    } else {
      setMessage(data.message);
    }
  };

  return (
    <div className="form-container-login">
    <div className="form-content-left">
    <img src={imageSrc} alt="My" />
    </div>
    <div class="form-content-right">
    <div className="form">
    <div class="logo-container">
    <img src={Logo} alt="My" class="logo"/>
    </div>
    <h1>Connexion</h1> 
    <form>
    <div class="input-container-login">
    <FaUser className='icon'/>
    <input
        className='input-login'
        class="username-input-login"
        type="text"
        id="nom_prenom"
        placeholder="Nom et prénom"
        value={nomPrenom}
        onChange={(e) => setNomPrenom(e.target.value)}
      />
   
     </div> <br/>
    <div class="input-container-login">
    <FaLock  alt="password" className='icon'/>
    <input
      className='input-login'
      class="username-input-login"
        type="password"
        placeholder="Mot de passe"
        value={motDePasse}
        onChange={(e) => setMotDePasse(e.target.value)}
      />  </div>
     <br/><br/>
    <button  className='submit-login' type="button" onClick={handleLogin}>Se connecter</button>

    </form>
    {message && <p>{message}</p>}
    </div>
    </div>
    </div>
);
}

export default Login;
