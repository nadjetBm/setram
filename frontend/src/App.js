import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import Login from './Login';
import Rejeu from './Components/ModuleRejeu/Rejeu';
import FrequentationNbValJour from './Components/ModuleFrequentation/FrequentationNbValJour';
import DashboardAdmin from './Components/Admin/DashboardAdmin';
import DashboardUser from './Components/Common/DashboardUser';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Login/>}/> 
        <Route path='/rejeu' element={<Rejeu/>}/>
        <Route path='/nb' element={<FrequentationNbValJour/>}/>
        <Route path='/dashboard' element={<DashboardAdmin />} />
        <Route path='/dashboardUser' element={<DashboardUser/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;